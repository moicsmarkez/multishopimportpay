jQuery(document).ready(function() {
    
        jQuery('#form_clients input').on('input', function () {
            var boton = jQuery('#check_client');
            boton.prop("disabled", false); 
            boton.val('✔');
        });
    
    
        jQuery('body').on('click', 'div.wrap input[id^="check_client"]', function () {
	    var data = jQuery(jQuery(jQuery( this ).parent().get( 0 )).parent().get( 0 )).find(':input').serializeArray() ;
        data[data.length] = { name: 'check_client_nonce', value: jQuery('input[name="check_client_nonce"]').val() } ;
        
        var boton = jQuery(this);
        var action = 'admin-ajax.php?action=check_client_api';
        var method = 'post';

        jQuery.ajax({
            url: action,
            method: method,
            data: data,
        beforeSend: function() {
            console.log('Updating Field');
            jQuery(this).attr('disabled', 'disable');
            jQuery('#form_clients').block({message: null,overlayCSS: {background: '#e4e4e4',opacity: 0.6}});
        },
        success : function( response ) {
             console.log('Success ' );
             if(response.success){
                 boton.removeClass('button-error');
                 boton.val('✔');
                 boton.attr('disabled', 'disable');
             }else {
                 boton.addClass('button-error');
                 boton.val(response.data);
             }
        },
        }).done(function (data) {
            jQuery(this).removeAttr('disabled');
            jQuery('#form_clients').unblock();
        })
    });


    jQuery('#form_clients').on('click', 'table.settings-class-s1 #rem_client', function () {
            if(jQuery('table.settings-class-s1 tr').length < 3){
                return;
            }
            
            jQuery(jQuery( this ).parent().get( 0 )).parent().get( 0 ).remove();
            
            if(jQuery('table.settings-class-s1 tr').length <= 10){
                 jQuery('#add_client').prop("disabled", false);
            }
         });
        
    jQuery('#form_clients').on('click', '#add_client', function () {
             
             var id_tag = Math.floor(Math.random()*16777215);
             var nueva_fila = '<tr><td><input type="text" required="" title="Este campo es requerido" name="nombre_t_'+ id_tag +'" placeholder="Vitrina 1" value="" ></td><td><input type="text" required="" pattern="\\S+" title="Este campo es requerido, no puede llevar espacio" name="vinculo_t_'+id_tag+'" placeholder="http://vitrina.com" value=""></td><td><input type="text" required="" pattern="\\S+" title="Este campo es requerido, no puede llevar espacio" name="clave_cliente_t_'+id_tag+'" placeholder="ck_29f129e73d7eb36677b9c18dba29ad6eff03cc1d" value=""></td><td><input type="text" required="" pattern="\\S+" title="Este campo es requerido, no puede llevar espacio" name="clave_s_cliente_t_'+id_tag+'" placeholder="cs_664b76f35d3657b7adba8262b96a3fe4013212ca" value=""></td><td style="display: none;" ><input type="hidden" name="factura_s_cliente_t_'+id_tag+'" value="0" /></td><td><input type="checkbox" title="El cliente emite factura" name="factura_s_cliente_t_'+ id_tag +'" value="checked" /></td><td style="text-align: center;"><input style="display: inline; width: 45%;" title="Verificar Conexion" type="button" value="✔" class="button button-warning" id="check_client"><input style="display: inline; width: 45%;" title="Eliminar tienda" type="button" value="✘" class="button button-warning" id="rem_client"></td></tr>';
             
             if(jQuery('table.settings-class-s1 tr').length > 10){
                 jQuery(this).attr('disabled', 'disable');
                 return;
            }
            jQuery('table.settings-class-s1 tr:last').after(nueva_fila);
        });
     
    jQuery('#tiendas_vitrinas').on('click', '#add_tiendas_vitrinas', function () {
            if(jQuery('#tiendas_vitrinas > div.custom_tab_options').length >= 2){
                return;
            }
              
            var clonar = jQuery('#tiendas_vitrinas > div.custom_tab_options').first();
            var exit_ ='';
            
            if(clonar.length >= 1) {
                jQuery('#tiendas_vitrinas > div.custom_tab_options select').each(function(){
                    if(jQuery(this,'option:selected').val() == ''){
                        exit_ = 1;
                    }
                });    
                if(exit_ > 0){return;}
                clonar.clone().appendTo(clonar.parent().get(0)).removeClass("copy-1").addClass("copy-"+jQuery('#tiendas_vitrinas > div.custom_tab_options').length).find('select').prop('selectedIndex',0).attr("name", "_tiendas_vitrinas_"+Math.floor(Math.random()*16777215));
            }
    });
    
    jQuery('#tiendas_vitrinas').on('click', 'input[id^="rem_vitrina"]', function () {
            if(jQuery('#tiendas_vitrinas > div.custom_tab_options').length < 2){
                jQuery(jQuery( this ).parent().get( 0 )).find('select').prop('selectedIndex',0);
                return;
            }
            
            jQuery(jQuery( this ).parent().get( 0 )).parent().get( 0 ).remove();
    });
    
    
    jQuery( "body" ).on( "click", 'div.wrap span[id^="vinculo_dia"]', function() {
        var panel_clickeado = jQuery(jQuery(this).parent().get(0)).parent().get(0);
        jQuery("#valor-vinculo").val(jQuery(panel_clickeado).find('td:nth-child(2) > input[type="hidden"]').val());
        jQuery("#host_id_current").text(jQuery(jQuery( this ).parent().get( 0)).find("span:nth-child(2)").text());
        jQuery("#vinculo_cliente_dg").dialog({
          show: { effect: "puff", duration: 195 },
          hide: { effect: "puff", duration: 150 },
          autoOpen: false,
          draggable: false,
          resizable: false,
          height: "auto",
          width: 600,
          modal: true,
          buttons: {
            "Aceptar": function() {
              if(jQuery(panel_clickeado).find('td:nth-child(2) > input[type="hidden"]').val().trim() != jQuery('#valor-vinculo').val().trim() && jQuery('#valor-vinculo').val().trim().length != 0){
                 jQuery(panel_clickeado).find('td:nth-child(2) > input[type="hidden"]').val(jQuery('#valor-vinculo').val().trim());
                 jQuery(panel_clickeado).find('span#vinculo_dia').text('Guardar Cambios!');
                 jQuery(panel_clickeado).find('td:nth-child(3) > span:nth-child(2)').text('Por favor Guardar Cambios!');
              }
              jQuery( this ).dialog( "close" );
            },
            Cancel: function() {
              jQuery( this ).dialog( "close" );
            }
          }
        });
        jQuery( "#vinculo_cliente_dg" ).dialog( "open" );
        jQuery(".ui-dialog-titlebar").hide();
    });
    
     jQuery( "body" ).on( "click", 'div.wrap span[id^="cliente_key"]', function() {
        var panel_clickeado = jQuery(jQuery(this).parent().get(0)).parent().get(0);
         jQuery("#key_cliente_dg").dialog({
          show: { effect: "puff", duration: 195 },
          hide: { effect: "puff", duration: 150 },
          autoOpen: false,
          draggable: false,
          resizable: false,
          height: "auto",
          width: 600,
          modal: true,
          buttons: {
            "Aceptar": function() {
              if(jQuery('#value_key_c').val().trim().length != 0){
                  jQuery(panel_clickeado).find('td:nth-child(4) > input[type="hidden"]').val(jQuery('#value_key_c').val().trim());
                  jQuery(panel_clickeado).find('span#cliente_key').text('Guardar Cambios!');
                  jQuery('#value_key_c').val('');
              }
              jQuery( this ).dialog( "close" );
            },
            Cancel: function() {
              jQuery( this ).dialog( "close" );
            }
          }
        });
        jQuery( "#key_cliente_dg" ).dialog( "open" );
        jQuery(".ui-dialog-titlebar").hide();
    });
    
    jQuery( "body" ).on( "click", 'div.wrap span[id^="key_secrect_cliente"]', function() {
       var panel_clickeado = jQuery(jQuery(this).parent().get(0)).parent().get(0);
        jQuery("#secret_key_cliente_dg").dialog({
          show: { effect: "puff", duration: 195 },
          hide: { effect: "puff", duration: 150 },
          autoOpen: false,
          draggable: false,
          resizable: false,
          height: "auto",
          width: 600,
          modal: true,
          buttons: {
            "Aceptar": function() {
              if(jQuery('#value_key_c_s').val().trim().length != 0){
                  jQuery(panel_clickeado).find('td:nth-child(5) > input[type="hidden"]').val(jQuery('#value_key_c_s').val().trim());
                  jQuery(panel_clickeado).find('span#key_secrect_cliente').text('Guardar Cambios!');
                  jQuery('#value_key_c_s').val('');
              }
              jQuery( this ).dialog( "close" );
            },
            Cancel: function() {
              jQuery( this ).dialog( "close" );
            }
          }
        });
        jQuery( "#secret_key_cliente_dg" ).dialog( "open" );
        jQuery(".ui-dialog-titlebar").hide();
    
    });
    
     jQuery( "body" ).on( "click", '#host_id_current', function() {
         
     });
    
});



                