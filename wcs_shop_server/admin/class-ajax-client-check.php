<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      0.0.1
 *
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/admin
 * @author     Your Name <email@example.com>
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit


if ( ! class_exists( 'wcs_shop_server_ajax_client_check' ) ) :

   class wcs_shop_server_ajax_client_check {
       
       function __construct(){
            add_action( 'wp_ajax_check_client_api', array(__CLASS__,'chequear_cliente'));
        }
        
        public static function chequear_cliente(){
            
            foreach($_POST as $key => $value) {
                if (strpos($key, 'vinculo_t') === 0) { $vinculo = !empty($value) ? sanitize_text_field($value) : '';}
                if (strpos($key, 'clave_cliente_t') === 0) { $claveCliente = !empty($value) ? sanitize_text_field($value) : '';}
                if (strpos($key, 'clave_s_cliente_t') === 0) { $claveSecretaCliente = !empty($value) ? sanitize_text_field($value) : '';}
            }
     
   		 
   		 if(!(wp_verify_nonce( $_POST['check_client_nonce'], 'api_checking_client') && is_admin())) {
            wp_send_json_error('Muerte 1: No te haz logeado, o tratas de entrar sin autorizacion');
            die(-1);
          }
          
          $client = apply_filters('wcs_shop_server_api_action', $vinculo, $claveCliente, $claveSecretaCliente);
          
   		 if(!($client === 'ERROR')){
   		    try {
              $resultado = $client->customers->get('',array( 'filter[limit]' => 1 ));
   		      wp_send_json_success('✔'); 
   		      wp_die();
             }catch (WC_API_Client_Exception $e) {
               wp_send_json_error('⚠');
               wp_die();
             }
          }else {
   		    wp_send_json_error('No!');
   		    wp_die();
   		 }
        }
   }

endif;

return new wcs_shop_server_ajax_client_check();
