
<div class="wrap">
            <h1 style="font-size: 30px;font-weight: 900;text-shadow: 3px 3px 5px #929292;" ><?php echo esc_html( get_admin_page_title() ); ?></h1>
            <h3><?php  _e('Introduce las opciones de las tiendas clientes','wc-total-shop'); ?></h3>

            <?php  do_action( 'get_s_settings_messages' ); ?>
            
            <form method="post" action="<?php echo esc_html( admin_url( 'admin-post.php' ) ); ?>" id="form_clients" >
                <div style="text-align: right;">
                    <input id="add_client" type="button" value="+ Agregar Tienda" style="display: inline-block;background-color: #008046;margin: 1px 2px;color: white;font-weight: bold;" class="button btn">
                </div>
              <table class="settings-class-s1">
                <tbody>
                    <tr>
                        <td>Nombre</td>
                        <td>Pagina</td>
                        <td>Clave del cliente	</td>
                        <td>Clave secreta de cliente</td>
                        <td>Facturación</td>
                        <td>Acciones</td>
                    </tr>
                    <?php
                    
                    
                        $opciones_cliente = wcs_shop_server_deserializer::getInstance()->get_value('opciones_clientes');
                       
                        
                       if(!empty($opciones_cliente)){
                        
                         foreach($opciones_cliente as $opciones => $opcion){
                            $opcion_keys = array_keys($opcion);
                            
                            ?>
                                <tr>
                                    <td><input type="text" required title="Este campo es requerido" name="<?php echo $opcion_keys[0] ?>" placeholder="Vitrina 1" value="<?php echo $opcion[$opcion_keys[0]] ?>" /></td>
                                    <td style="display: none"><input type="hidden" name="<?php echo $opcion_keys[2] ?>" value="<?php echo $opcion[$opcion_keys[2]]  ?>" /></td>
                                    <td>
                                        <span id="vinculo_dia" class="noway" title="Editar Vinculo, Verificar Host ID" data-vinculo="<?php echo $opcion[$opcion_keys[2]] ?>" ><?php echo substr($opcion[$opcion_keys[2]], 0, strlen($opcion[$opcion_keys[2]])/2 ).'....';  ?></span>
                                        <span style="display: none"><?php echo $opcion[$opcion_keys[1]] ?></span>
                                    </td>
                                    <td style="display: none"><input type="hidden" name="<?php echo $opcion_keys[4] ?>" value="<?php echo $opcion[$opcion_keys[4]]  ?>" /></td>
                                    <td style="display: none"><input type="hidden" name="<?php echo $opcion_keys[3] ?>" value="<?php echo $opcion[$opcion_keys[3]]  ?>" /></td>
                                    <td><span id="cliente_key" class="noway" title="Haz click para modificar" ><?php echo '...'.substr($opcion[$opcion_keys[4]], -8 , 8);  ?> </span></td>
                                    <td><span id="key_secrect_cliente" class="noway" title="Haz click para modificar" ><?php echo '...'.substr($opcion[$opcion_keys[3]], -8 , 8);  ?> </span></td>
                                    <td style="display:none;"><input type="hidden" name="<?php echo $opcion_keys[5] ? $opcion_keys[5] : 'factura_s_cliente_t_'.intval(preg_replace('/[^0-9]+/', '', $opcion_keys[4]), 10); ?>" value="0" /></td>
                                    <td><input type="checkbox" title="El cliente emite factura" name="<?php echo $opcion_keys[5] ? $opcion_keys[5] : 'factura_s_cliente_t_'.intval(preg_replace('/[^0-9]+/', '', $opcion_keys[4]), 10); ?>" value="checked"  <?php  checked('checked', $opcion[$opcion_keys[5]]);   ?> /></td>
                                    <td style="text-align: center;">
                                        <input style="display: inline; width: 45%;" title="Verificar Conexion"  type="button" value="&#10004" class="button button-warning" id="check_client" />
                                        <input style="display: inline; width: 45%;" title="Eliminar tienda"  type="button" value="&#10008" class="button button-warning" id="rem_client" />
                                    </td>
                                </tr>   
                            <?php
                            $opcion_keys = null;
                            }
                        }else {
                            ?>
                             <tr>
                                <td><input type="text" required title="Este campo es requerido" name="nombre_t_1" placeholder="Vitrina 1" value="" /></td>
                                <td><input type="text" required pattern="\S+" title="Este campo es requerido, no puede llevar espacio" name="vinculo_t_1" placeholder="http://vitrina.com" value="" /></td>
                                <td><input type="text" required pattern="\S+" title="Este campo es requerido, no puede llevar espacio" name="clave_cliente_t_1" placeholder="ck_29f129e73d7eb36677b9c18dba29ad6eff03cc1d" value="" /></td>
                                <td><input type="text" required pattern="\S+" title="Este campo es requerido, no puede llevar espacio" name="clave_s_cliente_t_1" placeholder="cs_664b76f35d3657b7adba8262b96a3fe4013212ca" value="" /></td>
                                <td><input type="checkbox" title="El cliente emite factura" name="factura_s_cliente_t_1" value="" /></td>
                                <td style="text-align: center;">
                                    <input style="display: inline; width: 45%;" title="Verificar Conexion"  type="button" value="&#10004" class="button button-warning" id="check_client" />
                                    <input style="display: inline; width: 45%;" disabled="disable" title="Eliminar tienda"  type="button" value="&#10008" class="button button-warning" id="rem_client" />
                                </td>
                            </tr>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
            <div style="display: none" id="vinculo_cliente_dg" title="Editar Vinculo">
                <p><span style="font-size: 1.1em;font-weight: bolder;">Vinculo de la tienda cliente: </span> <input id="valor-vinculo" style="width: 350px;" autocomplete="off" type="text" /></p>
                <p><span style="font-size: 1.1em;font-weight: bolder;">Host id tienda cliente: </span><span id="host_id_current" style="font-weight: bolder; color: red;"></span> </p>
            </div>
            <div style="display: none" id="key_cliente_dg" title="Editar clave cliente">
                <p><span style="font-size: 1.1em;font-weight: bolder;">Nueva clave cliente: </span> 
                <input id="value_key_c" style="width: 350px;" placeholder="ck_29f129e73d7eb36677b9c18dba29ad6eff03cc1d" autocomplete="off"  type="text" /></p>
            </div>
            <div style="display: none" id="secret_key_cliente_dg" title="Editar clave secreta cliente">
                <p><span style="font-size: 1.1em;font-weight: bolder;">Nueva clave secreta cliente: </span>
                <input id="value_key_c_s" style="width: 350px;" placeholder="cs_29f129e73d7eb36677b9c18dba29ad6eff03cc1d" autocomplete="off"  type="text" /></p>
            </div>
            <input type="hidden" name="check_client_nonce" value="<?php echo wp_create_nonce('api_checking_client') ?>" />
            <?php
                wp_nonce_field( 'wcs-shop-server-settings-save', 'wcs-shop-server-prior' );
                @submit_button(); 
            ?>
            </form>
</div><!-- .wrap -->
