<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      0.0.1
 *
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/admin
 * @author     Your Name <email@example.com>
 */


 
class wcs_shop_server_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.0.1
	 * @access   private
	 * @var      string    $wcs_shop_server    The ID of this plugin.
	 */
	private $wcs_shop_server;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.0.1
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.0.1
	 * @param      string    $wcs_shop_server       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $wcs_shop_server, $version ) {
		
		$this->requires_incl();
		$this->wcs_shop_server = $wcs_shop_server;
		$this->version = $version;
		
	}

	private function requires_incl(){
		include_once( plugin_dir_path( __FILE__ ) . 'class-ajax-client-check.php' );
		include_once( plugin_dir_path( __FILE__ ) . 'menus/class-submenu.php' );
		include_once( plugin_dir_path( __FILE__ ) . '../shared/class-deserializer.php' );
		include_once( plugin_dir_path( __FILE__ ) . '../includes/class-serializer.php' );
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    0.0.1
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in wcs_shop_server_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The wcs_shop_server_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->wcs_shop_server, plugin_dir_url( __FILE__ ) . 'css/wcs-shop-server-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->wcs_shop_server.'jQueryUi', plugin_dir_url( __FILE__ ) . 'css/jquery-ui.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->wcs_shop_server.'jQueryUi', plugin_dir_url( __FILE__ ) . 'css/jquery-ui.theme.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    0.0.1
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in wcs_shop_server_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The wcs_shop_server_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->wcs_shop_server, plugin_dir_url( __FILE__ ) . 'js/wcs-shop-server-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->wcs_shop_server.'jqueryUi', plugin_dir_url( __FILE__ ) . 'js/jquery-ui.min.js', array( 'jquery' ), $this->version, false );
		wp_register_script('jquery-blockUi', plugin_dir_url( __FILE__ ) .'js/jquery.blockUI.js', array('jquery'));
	    wp_enqueue_script('jquery-blockUi');    

	}
	
	public function actualiza_orden_nueva($order_id){
		 $orden = wc_get_order( $order_id );
		 
		
		$data = array(
		    "order" =>array(
		    		 'status'  => $orden->get_status()=='pending' ? 'on-hold' : $orden->get_status(),
		    		 'currency' => $orden->get_currency(),
		    		 'note' => $orden->get_customer_note,
					 'customer_ip'  => '190.199.70.231',
					 'customer_user_agent' => $orden->get_customer_user_agent(),
					 'view_order_url' => $orden->get_view_order_url(),
				    'payment_details' => array(
			            'method_id' => $orden->get_payment_method(),
			            'method_title' => $orden->get_payment_method_title(),
			            'paid' => ! is_null( $orden->get_date_paid() ),
			        ),'billing_address' => array(
						'first_name' => $orden->get_billing_first_name(),
						'last_name'  => $orden->get_billing_last_name(),
						'company'    => $orden->get_billing_company(),
						'address_1'  => $orden->get_billing_address_1(),
						'address_2'  => $orden->get_billing_address_2(),
						'city'       => $orden->get_billing_city(),
						'state'      => $orden->get_billing_state(),
						'postcode'   => $orden->get_billing_postcode(),
						'country'    => $orden->get_billing_country(),
						'email'      => $orden->get_billing_email(),
						'phone'      => $orden->get_billing_phone(),
					),
				    'shipping_address' => array(
						'first_name' => $orden->get_shipping_first_name(),
						'last_name'  => $orden->get_shipping_last_name(),
						'company'    => $orden->get_shipping_company(),
						'address_1'  => $orden->get_shipping_address_1(),
						'address_2'  => $orden->get_shipping_address_2(),
						'city'       => $orden->get_shipping_city(),
						'state'      => $orden->get_shipping_state(),
						'postcode'   => $orden->get_shipping_postcode(),
						'country'    => $orden->get_shipping_country(),
					),
					'total' => wc_format_decimal( $orden->get_total(), 2 ),
					'subtotal' => wc_format_decimal( $orden->get_subtotal(), 2 ),
					'total_line_items_quantity' => $orden->get_item_count(),
					'total_tax' => wc_format_decimal( $orden->get_total_tax(), 2 ),
					'total_shipping' => wc_format_decimal( $orden->get_shipping_total(), 2 ),
					'cart_tax' => wc_format_decimal( $orden->get_cart_tax(), 2 ),
					'shipping_tax' => wc_format_decimal( $orden->get_shipping_tax(), 2 ),
					'total_discount' => wc_format_decimal( $orden->get_total_discount(), 2 ),
				  )
		    );
		
			// add shipping
			foreach ( $orden->get_shipping_methods() as $shipping_item_id => $shipping_item ) {
				$data['order']['shipping_lines'][] = array(
					'id'           => $shipping_item_id,
					'method_id'    => $shipping_item->get_method_id(),
					'method_title' => $shipping_item->get_name(),
					'total'        => wc_format_decimal( $shipping_item->get_total(), 2 ),
				);
			}
			
			// add fees
			foreach ( $orden->get_fees() as $fee_item_id => $fee_item ) {
				$data['order']['fee_lines'][] = array(
					'id'        => $fee_item_id,
					'title'     => $fee_item->get_name(),
					'tax_class' => $fee_item->get_tax_class(),
					'total'     => wc_format_decimal( $orden->get_line_total( $fee_item ), 2 ),
					'total_tax' => wc_format_decimal( $orden->get_line_tax( $fee_item ), 2 ),
				);
			}
			   
			// add coupons
			foreach ( $orden->get_items( 'coupon' ) as $coupon_item_id => $coupon_item ) {
			    $coupon_original = new WC_Coupon( $coupon_item->get_code() );
				$coupon_original = get_post_meta($coupon_original->get_id(), 'code_cupon_tienda_cliente', true);
				$data['order']['coupon_lines'][] = array(
					'id'     => intval(get_post_meta($coupon_item_id, 'id_cupon_tienda_cliente', true)),
					'code'   => strval(($coupon_original != null || $coupon_original!='') ? $coupon_original : $coupon_item->get_code()),
					'amount' => wc_format_decimal( $coupon_item->get_discount(), 2 ),
				);
			}

			$tienda_cliente = array();
			
			// add line items falsos pasa el id servidor!
			foreach( $orden->get_items() as $item_id => $item ) {
				$product    = $item->get_product();
				
				$tienda_cliente = get_post_meta($product->id, 'wcs_shop_server__tienda_vitrina',  TRUE);
				$host_parent_id = get_post_meta($product->id, 'wcs_shop_server_id_product_'.$tienda_cliente['host'],  TRUE);
				
				$data['order']['line_items_wcs_shop_server_id'][] = array(
					'id_host'    => intval($host_parent_id),
					'quantity'   => $item->get_quantity(),
					'subtotal' => wc_format_decimal( $orden->get_line_subtotal( $item, false, false ), 2 ),
					'subtotal_tax' => wc_format_decimal( $item->get_subtotal_tax(), 2 ),
					'total' => wc_format_decimal( $orden->get_line_total( $item, false, false ), 2 ),
					'total_tax' => wc_format_decimal( $item->get_total_tax(), 2 ),
					'price' => wc_format_decimal( $orden->get_item_total( $item, false, false ), 2 )
				);
			}
			
			$vitrinas = wcs_shop_server_deserializer::getInstance()->get_value('opciones_clientes');
			$key_v = $this->busquedaURLopciones($tienda_cliente['host'], $vitrinas);
	 		$vitrina_sl= array_keys($vitrinas[$key_v]);
	 		// print_r($valor['site_url']);
	 		$client_api = apply_filters('wcs_shop_server_api_action', $vitrinas[$key_v][$vitrina_sl[2]], $vitrinas[$key_v][$vitrina_sl[4]], $vitrinas[$key_v][$vitrina_sl[3]]); 
			
			
		 	
			//AGREGA LA ORDEN A LA TIENDA CLIENTE
			 
	         if(!($client_api === 'ERROR')){
	           try {
	           		if($orden->get_user_id() > 0){
	           			$data_usuario =  $client_api->customers->get_by_email(get_userdata($orden->get_user_id())->user_email);
	           			if(!empty($data_usuario) && count($data_usuario['customer']) > 0){
		           			$data['order']['customer_id']= $data_usuario['customer']['id'];
		           			$data_usr = array(
				             'customer' => array(
				                 'billing_address' => array(
				                    	'first_name' => $orden->get_billing_first_name(),
											'last_name'  => $orden->get_billing_last_name(),
											'company'    => $orden->get_billing_company(),
											'address_1'  => $orden->get_billing_address_1(),
											'address_2'  => $orden->get_billing_address_2(),
											'city'       => $orden->get_billing_city(),
											'state'      => $orden->get_billing_state(),
											'postcode'   => $orden->get_billing_postcode(),
											'country'    => $orden->get_billing_country(),
											'email'      => $orden->get_billing_email(),
											'phone'      => $orden->get_billing_phone(),
				                 ),
				                 'shipping_address' => array(
				                     'first_name' => $orden->get_shipping_first_name(),
											'last_name'  => $orden->get_shipping_last_name(),
											'company'    => $orden->get_shipping_company(),
											'address_1'  => $orden->get_shipping_address_1(),
											'address_2'  => $orden->get_shipping_address_2(),
											'city'       => $orden->get_shipping_city(),
											'state'      => $orden->get_shipping_state(),
											'postcode'   => $orden->get_shipping_postcode(),
											'country'    => $orden->get_shipping_country(),
				                 )
				             )
				         ); 
				         	$data_usuario =  $client_api->customers->update($data_usuario['customer']['id'], $data_usr);
		           		}
		           		// $data_usuario=null;
	           		}
	           		
	            	$client_api->custom->setup('tienda');
	            	$return_email = $vitrinas[$key_v][$vitrina_sl[5]]==='checked' ? $client_api->custom->get('clienteconfactura/sip') : $client_api->custom->get('clienteconfactura/nop');  
	            	$ready = $client_api->custom->post('ordenes', $data);
						if(count($ready['order']) > 0 && !empty($ready['order'])){
							wp_destroy_current_session();
							update_post_meta($orden->get_id(), '_cliente_1_order_key_value', $ready['order']['order_key']);
							update_post_meta($orden->get_id(), '_cliente_1_order_id', $ready['order']['id']);
							$estados = array( 'customer_on_hold_order' => 'on-hold', 'failed_order' => 'failed', 'cancelled_order' => 'cancelled','customer_processing_order' => 'processing' , 'customer_completed_order' => 'completed');
		           		if($vitrinas[$key_v][$vitrina_sl[5]]==='checked'){
		           				if ( ! empty( WC()->mailer()->get_emails() ) ) {
								    foreach ( WC()->mailer()->get_emails() as $mail ) {
								        if ( $mail->id ==  strval(array_search( $orden->get_status()=='pending' ? 'on-hold' : $orden->get_status(),$estados) )) {
								        	  $mail->enabled = 'no';
								        }
								     }
									}	
								$return_email = $return_email['res'] == true ? $client_api->custom->get('emailfactura/'.$ready['order']['id'],array( 'filter[email_type]' => array_search( $orden->get_status()=='pending' ? 'on-hold' : $orden->get_status(),$estados))) : false; 
							}
							
						}else {
							wc_add_notice( sprintf( __('ERROR EN TIENDA CLIENTE.'), 'error' ));	
						}
	                 //wc_add_notice( sprintf( __('/Por AQUI PASO. '.intval(get_post_meta(331, 'id_cupon_tienda_cliente', true)))), 'notice' );
	             } catch (WC_API_Client_Exception $e) {
	                wc_add_notice( sprintf( __('OCURRIO UN ERROR EN LA CONEXION CON LA TIENDA CLIENTE. ' ), 'error' ));
	                //print_r($e);
	             }
	          }else {
	          	    wc_add_notice( sprintf( __('OCURRIO UN ERROR ANTES DE LA TIENDA CLIENTE.' ), 'error' ));
	          }  
		}
		
		public function verifique_gracias($order_id){
			//VERIFICAR LAS OPCIONES DE LA TIENDA SI ESTA PERMITE FACTURA
			
			  $order = wc_get_order($order_id);
		   	  if ( $order ) {
		   	  	 $tienda_cliente = array();
		    		 foreach( $order->get_items() as $item_id => $item ) {
						$product    = $item->get_product();
						$tienda_cliente[] = get_post_meta($product->id, 'wcs_shop_server__tienda_vitrina',  TRUE);
					 }
		    		 if(count($tienda_cliente) < 1 && empty($tienda_cliente[0])){
		    		 	unset($tienda_cliente);
		    		 	return;
			 		 }
			 		 $vitrinas = wcs_shop_server_deserializer::getInstance()->get_value('opciones_clientes');
					 $key_v = $this->busquedaURLopciones($tienda_cliente[0]['host'], $vitrinas);
	 				 $vitrina_sl= array_keys($vitrinas[$key_v]);
			 		 
			 		 if($vitrinas[$key_v][$vitrina_sl[5]] ==='checked'){
				 		 $url_tienda = esc_url($tienda_cliente[0]['url-shop']);
				 		 unset($tienda_cliente);
				 		 wp_redirect($url_tienda.'?key_cliente_order='.esc_attr(get_post_meta($order_id,'_cliente_1_order_key_value' ,true)));
						 exit();
			 		 }
		   	  }
		}
		
	
	//FUNCION CAPTURAR CARRITO QUE VIENE DE LA VITRINA
	public function capturar_carrito(){
		
		if(is_front_page()){
			if((isset($_POST['p_cart']) && !empty($_POST['p_cart']) && $_POST['p_cart'] != '') && (isset($_POST['carrito_key']) && !empty($_POST['carrito_key']) && $_POST['carrito_key'] != '')){
				 //$contenido=unserialize(base64_decode($_POST["p_cart"]));
				 $contenido=unserialize($this->encrypt_decrypt('decrypt', $_POST['p_cart'], $_POST['carrito_key']));
				 $vitrinas = wcs_shop_server_deserializer::getInstance()->get_value('opciones_clientes');
				 
				 WC()->cart->empty_cart();
				 WC()->cart->remove_coupons();
				 
			 	 if(!$this->in_array_r($contenido['host_id'], $vitrinas)){
			 		echo '<h1>ERROR DE COMUNICACION, OBJETO INVALIDO</h1>' ;
			 		exit();
			 	 }
				 
				 $key_v = $this->busquedaURLopciones($contenido['host_id'], $vitrinas);
 		 		 $vitrina_sl= array_keys($vitrinas[$key_v]);
		 		 // print_r($valor['site_url']);
		 		 $client_api = apply_filters('wcs_shop_server_api_action', $vitrinas[$key_v][$vitrina_sl[2]], $vitrinas[$key_v][$vitrina_sl[4]], $vitrinas[$key_v][$vitrina_sl[3]]); 
				 
				 //FUNCIONES PARA EL USUARIO 
				 if(isset($contenido['logged'])){
					 if(!email_exists($contenido['logged'])){
					 		//CREAR NUEVO USUARIO CON LA INFORMACION DEL CLIENTE!
					 		if(!($client_api === 'ERROR')){
	    		 			 try {
	    		 			   	$data_usuario =  $client_api->customers->get_by_email($contenido['logged']);
	    		 			   	$usuario_id = $this->create_customer_wcs($data_usuario, $contenido['host_id']);
	    		 			   	// $data_usuario=null;
	    		 			   	if($usuario_id > 0){
	    		 			   		//HACER AUTOLOGIN
	    		 			   		$user = get_user_by('id', $usuario_id);
	    		 			   		wp_set_current_user($user->ID);
	    		 			   		wp_set_auth_cookie($user->ID);
										do_action('wp_login', $user->user_login, $user);
										// $user=null;
	    		 			   	}
	            		 }catch(WC_API_Client_Exception $e){
	    		 			 	echo 'ERROR '.$e;
	    		 			 }
	    		 		}else {
	    		 			echo 'HAY ERRORES EN EL CLIENTE';
	    		 		}
				 	}else{
				 		//BUSCA Y LOGEA EL USUARIO EXISTENTE
				 		$usuario_id = email_exists( $contenido['logged']);
				 		$user = get_user_by('id', $usuario_id);
	    		 			   		wp_set_current_user($user->ID);
	    		 			   		wp_set_auth_cookie($user->ID);
										do_action('wp_login', $user->user_login, $user);
										// $user=null;
				 	}
				 }else{
				 	wp_destroy_current_session();
				 }
				 
				 
				 foreach($contenido as $key => $valor){
				    if(!is_numeric($key)){
				 		continue;
				 	}
				 	
				 	$product_id = $this->get_wp_post_id_from_meta('wcs_shop_server_id_product_'.$contenido['host_id'], $valor['wcs_shop_server_product_id']);
				 	if(!$product_id){
				 		if(!($client_api === 'ERROR')){
				 			 try {
				 			 	$client_api->custom->setup('tienda');
				 			 	$producto_clitente =  $client_api->custom->get('product/'.intval($valor['wcs_shop_server_product_id']));
				 			 	if($this->get_product_by_sku($producto_clitente['sku'])>0 && !empty($producto_clitente['sku'])){
									continue;
								}
								$new_post_id = $this->import_product($producto_clitente['products'][0], $contenido['host_id'], $contenido['site_url'], $vitrinas[$key_v][$vitrina_sl[2]]);
								
								$product_id = $new_post_id > 0 ? $new_post_id : 0;
								// $new_post_id=null;
				 			 }catch(WC_API_Client_Exception $e){
				 			 	echo 'ERROR '.$e;
				 			 }
				 		}else {
				 			echo 'HAY ERRORES EN EL CLIENTE';
				 		}
				 	}else{
					 		if(!($client_api === 'ERROR')){
					 			 try {
					 			 	$client_api->custom->setup('tienda');
					 			 	$producto_clitente =  $client_api->custom->get('product/'.intval($valor['wcs_shop_server_product_id']));
					 			 	$producto = wc_get_product($product_id);
					 			 	if($producto->get_regular_price() !=$producto_clitente['products'][0]['regular_price']){
					 			 		$producto->set_regular_price($producto_clitente['products'][0]['regular_price']);
					 			 		$producto->save();
					 			 	}
					 			 	if($producto->get_sale_price() !=$producto_clitente['products'][0]['sale_price']){
					 			 		$producto->set_sale_price($producto_clitente['products'][0]['sale_price']);
					 			 		$producto->save();
					 			 	}
									unset($producto);
					 			 }catch(WC_API_Client_Exception $e){
					 			 	echo 'ERROR '.$e;
					 			 }
					 		}else {
					 			echo 'HAY ERRORES EN EL CLIENTE';
					 		}	
				 	}
				 	 WC()->cart->add_to_cart($product_id,$valor['cantidad']);
				 }
				 //AGREGA CUPONES QUE VIENEN DE LA VITRINA
				 if(!($client_api === 'ERROR')){
    		 			 try {
    		 			    if(count($contenido['cupon_descuento']) > 0){ 
        		 			    foreach($contenido['cupon_descuento'] as $code => $id){
        		 			        
        		 			        $coupon_id = $this->get_wp_post_id_from_meta('wcs_shop_server_cupon_tienda_cliente_'. $contenido['host_id'], $code);
        		 			        if(!$coupon_id){
        		 			        	$cupones = $this->get_query_coupons( );
        		 			        	
        		 			        	if(!empty($cupones)){
	        		 			        	foreach($cupones as $coupon){
	        		 			        		if($code === $coupon->post_title){
	        		 			        			$code = $code.'_'.$contenido['host_id'];
	        		 			        		}
	        		 			        	}
        		 			        	}
        		 			        }
        		 			        
        		 			        
        		 			        
        		 			        $validando = new WC_Coupon($coupon_id);
        		 			        if(!$validando->is_valid()){
        		 			            $get_coupon =  $client_api->coupons->get(intval($id));
            		 			        $cupon_nuevo = $this->nuevo_cupon($get_coupon['coupon'], $get_coupon['coupon']['code'] === $code ? null : $code, $contenido['host_id']);
            		 			        if($cupon_nuevo > 0){
            		 			            WC()->cart->add_discount($get_coupon['coupon'] === $code ? $get_coupon['coupon']  : $code);//FALTA HACER VERIFICACION DE IDS Y CAMBIAR EL CODE SI ESTE ACTUALIZO SU PORCENTAJE
            		 			        }        
        		 			        } else {
    		 			                WC()->cart->add_discount(get_the_title($coupon_id));   
        		 			        }
        		 			        //$validando=null;
        		 			    }
    		 			    }
    		 			 }catch(WC_API_Client_Exception $e){
    		 			 	echo 'ERROR '.$e;
    		 			 } 
    		 		}else {
    		 			echo 'HAY ERRORES EN EL CLIENTE';
    		 		}
				
				 //$client_api=null;
				 wp_cache_flush();
				 wp_redirect( WC()->cart->get_cart_url());
				 exit();
			}
		}
		if ( is_checkout() ) {
			if('yes' === get_option( 'woocommerce_enable_signup_and_login_from_checkout' )){
				 update_option( 'woocommerce_enable_signup_and_login_from_checkout', 'no' );
			}
		}
	}
	
	//FUNCION PARA AGREGAR CUPON DE LA TIENDA VITRINA
	private function nuevo_cupon($cupon = array(), $codeNuevo = '', $host_id){
	    if(empty($cupon)){
	        return ;
	    }
	    
	    //$coupon_code = $cupon['code']; // Code
        //$amount = $cupon['amount']; // Amount
        //$discount_type = $cupon['type'] ; // Type: fixed_cart, percent, fixed_product, percent_product
        					
        $coupon = array(
        	'post_title' => isset($codeNuevo) && !empty($codeNuevo) ? $codeNuevo : $cupon['code'],
        	'post_content' => '',
        	'post_status' => 'publish',
        	'post_author' => 1,
        	'post_type'		=> 'shop_coupon'
        );
        					
        $new_coupon_id = wp_insert_post( $coupon );
        					
        // Add meta
        update_post_meta( $new_coupon_id, 'discount_type',  $cupon['type']);
        update_post_meta( $new_coupon_id, 'coupon_amount', $cupon['amount'] );
        update_post_meta( $new_coupon_id, 'individual_use', $cupon['individual_use'] );
        update_post_meta( $new_coupon_id, 'product_ids', '' );//MOMENTANEAMENTE
        update_post_meta( $new_coupon_id, 'exclude_product_ids', '' );//MOMENTANEAMENTE
        update_post_meta( $new_coupon_id, 'usage_limit', $cupon['usage_limit'] );
        update_post_meta( $new_coupon_id, 'expiry_date', $cupon['expiry_date'] );
        update_post_meta( $new_coupon_id, 'apply_before_tax', 'yes' );
        update_post_meta( $new_coupon_id, 'free_shipping', 'no' );
        update_post_meta( $new_coupon_id, 'exclude_sale_items', $cupon['exclude_sale_items'] );
        update_post_meta( $new_coupon_id, 'wcs_shop_server_cupon_tienda_cliente_'.$host_id, $cupon['code'] );
        update_post_meta( $new_coupon_id, 'id_cupon_tienda_cliente', $cupon['id'] );
        if($coupon['post_title'] != $cupon['code']){ update_post_meta( $new_coupon_id, 'code_cupon_tienda_cliente', $cupon['code'] );}
        
        return $new_coupon_id;
	}
	
	
	
	private function busquedaURLopciones($url, $array) {
	   foreach ($array as $key => $val) {
	       if ($val['seudonimo_t'] == $url) {
	           return $key;
	       }
	   }
	   return null;
	}
	
	private function in_array_r($needle, $haystack, $strict = false) {
	    foreach ($haystack as $item) {
	        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
	            return true;
	        }
	    }
		return false;
	}
	
//FUNCIONES DE IMPORTACION DEBAJO DE ESTAS LINEAS ¡========================================
	private function insertar_categorias_producto($categorias_array = array(), $product_id){
		if(count($categorias_array) < 1 && empty($categorias_array)){
			return false;
		}
		
		$product_id =  wc_get_product($product_id);
		$product_id->set_category_ids( $categorias_array );
		$product_id->save();
		return true;
	}


	/**
	* Import a product
	*
	* @param array $product Product data
	* @param int $language Language ID
	* @return int New post ID
	*/
	private function import_product($product, $id_host, $url_shop, $url_site) {
			$product_medias = array();
			$post_media = array();
			$product_types = $this->create_woocommerce_product_types();
			

			// Date
			$date = $product['created_at'];

			// Product images
			if ( true ) {

				$images = $product['images'];
				foreach ( $images as $image ) {
						// foreach ($key as $image){
						$image_name = !empty($image['title'])? $image['title'] : $product['title'];
						$image_src =  $image['src'] ; // Get the potential filenames
						$media_id = $this->guess_import_media($image_name, $image_src, $date, ($this->img_count_media()+1));//$image['id'] id de la imagen desde donde proviene el producto		
						if ( $media_id !== FALSE ) {
							$product_medias[] = $media_id;
						}
					// }
				}
				// $this->media_count += count($product_medias); conteo de imagenes
				
				// Import content media
				$result = $this->import_media_from_content($product['description'], $date, $url_site);
				$post_media = $result['media'];
				// $this->media_count += $result['media_count'];
			}

			// Product categories
			$categories_ids = array();
			$product_categories = $product['categories'];
			foreach ( $product_categories as $cat_value ) {
				// if(count($this->categorias_importadas_productos()) > 0){
						$categoria_nueva = get_term_by('name',$cat_value, 'product_cat');
						if($categoria_nueva){
							// $categories_ids[] = get_term_by('name',$cat_value, 'product_cat')->term_id;
							$categories_ids[] = $categoria_nueva->term_id;
						}else {
							$term_nuevo = wp_insert_term( $cat_value, 'product_cat', [
								'description'=> $cat_value.' Category description imported',
								'slug' => strtolower ($cat_value).'-category' ]
							);
							if ( is_wp_error( $term_nuevo ) ) {
								$term_id = isset($term_nuevo->error_data['term_exists']) ? $term_nuevo->error_data['term_exists']:  null;
							} else {
								$categories_ids[] = $term_nuevo['term_id'];
							}
						}
						
			  }		
				
				//INSERTA EL HOST ID DE LA TIENDA COMO CATEGORIA
					$categoria_host_id = get_term_by('name',$id_host, 'product_cat');
					if($categoria_host_id ){
						$categories_ids[] = $categoria_host_id->term_id;
					}else {
						$term_host = wp_insert_term( $id_host, 'product_cat', [
								'description'=> $id_host.' es el identificador de la tienda que puede servir para reorganizar los costos de envio',
								'slug' => strtolower ($id_host).'-category' ]
						);
						if ( is_wp_error( $term_host ) ) {
							$term_host_id = isset($term_host->error_data['term_exists']) ? $term_host->error_data['term_exists']:  null;
						} else {
							$categories_ids[] = $term_host['term_id'];
						}
					}
			

			// Process content
			$content = isset($product['description'])? $product['description'] : '';
			$content = $this->process_content($content, $post_media);
			$excerpt = isset($product['short_description'])? $product['short_description'] : '';

			// Insert the post
			$new_post = array(
				'post_content'		=> $content,
				'post_date'			=> $date,
				'post_excerpt'		=> $excerpt,
				'post_status'		=> $product['status'],
				'post_title'		=> $product['title'],
				'post_name'			=> $product['title'],
				'post_type'			=> 'product',
			);

			
			$new_post_id = wp_insert_post($new_post);

			if ( $new_post_id ) {
				
					wp_set_object_terms($new_post_id, array_unique( array_map( 'intval', $categories_ids ) ), 'product_cat');
			
                if($product['type']=='variable'){
                    $product_type = $product_types['variable'];;
                    $manage_stock = 'no';
            
                }else if($product['type']=='simple'){
                    $product_type = $product_types['simple'];;
                    $manage_stock = 'yes';
            
                }
                
                wp_set_object_terms($new_post_id, intval($product_type), 'product_type', TRUE);

				// Product galleries
				$medias_id = array();
				foreach ($product_medias as $media) {
					$medias_id[] = $media;
				}
				if ( true ) {
					// Don't include the first image into the product gallery
					array_shift($medias_id);
				}
				
				$gallery = implode(',', $medias_id);

				// Prices
				
				// SKU = Stock Keeping Unit
				$sku = $product['sku'];
				
				// Stock
				$manage_stock =  'no';/*$product['managing_stock']? 'yes':*/
				
				$stock_status =  ($product['in_stock']) ? 'instock': 'outofstock';

				// Backorders
				
				// Add the meta data
				add_post_meta($new_post_id, '_visibility', $product['visible'], TRUE);
				add_post_meta($new_post_id, '_stock_status', $stock_status, TRUE);
				add_post_meta($new_post_id, '_regular_price', $product['regular_price'], TRUE);
				add_post_meta($new_post_id, '_price', $product['price'], TRUE);
				add_post_meta($new_post_id, '_sale_price', $product['sale_price'], TRUE);
				add_post_meta($new_post_id, '_featured', $product['featured'], TRUE);
				add_post_meta($new_post_id, '_width', floatval($product['dimensions']['width']), TRUE);
				add_post_meta($new_post_id, '_height', floatval($product['dimensions']['height']), TRUE);
				add_post_meta($new_post_id, '_sku', $sku, TRUE);
				add_post_meta($new_post_id, '_manage_stock', $manage_stock, TRUE);
				add_post_meta($new_post_id, '_product_image_gallery', $gallery, TRUE);
				add_post_meta($new_post_id, '_virtual', $product['virtual'], TRUE);
				add_post_meta($new_post_id, '_downloadable', $product['downloadable'], TRUE);
				add_post_meta($new_post_id, 'total_sales', $product['total_sales'], TRUE);
				add_post_meta($new_post_id, 'wcs_shop_server_id_product_'.$id_host, $product['id_servidor'], TRUE);
				add_post_meta($new_post_id, 'wcs_shop_server__tienda_vitrina', array('host' => $id_host, 'url-shop' => $url_shop), TRUE);
				
				foreach( array('exclude-from-catalog','exclude-from-search') as $valor_slug){
					wp_set_object_terms( $new_post_id, get_term_by('slug', $valor_slug, 'product_visibility')->term_id,'product_visibility', TRUE );	
				}
				 
				 if($product['type']=='variable'){
					//parent_id para manejar el id del post nuevo
	                $parent_id = $new_post_id;
	                    // agrego el nuevo atributo 
	                    
	                    $i=0;
	                    foreach ($product['attributes'] as $p_atributos) {
	                    	 $attibutos[$p_atributos['name']] = Array(
		                        'name'=> $p_atributos['name'],
		                        'value'=> implode("|",$p_atributos['options']),
		                        'position' => $i,
		                        'is_visible' => '1',
		                        'is_variation' => '1',
		                        'is_taxonomy' => '0'
		                    );
		                    $i++;
	                    }
	                    update_post_meta( $parent_id,'_product_attributes',$attibutos, TRUE);
	
	                $i=0;
	                foreach($product['variations'] as $variacion){
	                	
		                    $variation = array(
		                        'post_title'   => 'Atributos ' . $variacion['id'] . ' for #'.$parent_id ,
		                        'post_name' => 'product-' . $parent_id . '-variation-' . $i,
		                        'post_status'  => 'publish',
		                        'post_parent'  => $parent_id,
		                        'post_type'    => 'product_variation'
		                    );
		                    
		                    $attID = wp_insert_post( $variation );
		                    
		                    foreach ($variacion['attributes'] as $value) {
		                    	update_post_meta($attID, 'attribute_'.$value['name'], $value['option']);	
		                    }
		                    
		                    update_post_meta($attID, '_manage_stock', $variacion['managing_stock']=='parent' ?  $manage_stock : ($variacion['managing_stock']) ? 'yes' : 'no');
		                    update_post_meta($attID, '_stock', $variacion['stock_quantity']);
		                    update_post_meta($attID, '_sku', ($variacion['sku'] != $product['sku']) ? $variacion['sku'] : '' );
		                    update_post_meta($attID, '_stock_status',($variacion['in_stock']) ? 'instock' : 'outofstock');
		                    update_post_meta($attID, '_price', $variacion['price']);
		                    update_post_meta($attID, '_regular_price', $variacion['regular_price']);
		                    update_post_meta($attID, '_sale_price', $variacion['sale_price']);
		                    update_post_meta($attID, 'wcs_shop_server_id_product_'.$id_host, $variacion['id_servidor']);
		                    update_post_meta($attID, 'wcs_shop_server__tienda_vitrina', array('host' => $id_host, 'url-shop' => $url_shop));
		                    
	                    $i=$i+1;
	                }
				 }
				
				
				// Add links between the post and its medias
				$this->add_post_media($new_post_id, $product_medias, $date, TRUE);
				$this->add_post_media($new_post_id, $this->get_attachment_ids($post_media), $date, FALSE);
			}
			return $new_post_id;
		}

		public function categorias_importadas_productos(){
			$cat_args = array(
                'orderby'    => 'name',
                'order'      => 'asc',
                'hide_empty' => false,
            );
             
            return get_terms( 'product_cat', $cat_args );
		}
		
		/**
		 * Create the WooCommerce product types
		 *
		 * @return array Product types
		 */
		private function create_woocommerce_product_types() {
			$tab_types = array();
			$taxonomy = 'product_type';
			$product_types = array(
				'simple',
				'grouped',
				'variable',
				'external',
			);
			
			foreach ( $product_types as $product_type ) {
				$term = get_term_by('slug', $product_type, $taxonomy);
				if ( !empty($term) ) {
					$tab_types[$product_type] = $term->term_id;
				} else {
					$new_term = wp_insert_term($product_type, $taxonomy);
					if ( !is_wp_error($new_term) ) {
						$tab_types[$product_type] = $new_term['term_id'];
					}
				}
			}
			return $tab_types;
		}


		/**
		 * Import a media by guessing its name
		 * 
		 * @param string $image_name Image name
		 * @param array $image_filenames List of potential filenames
		 * @param date $date Media creation date
		 * @param int $image_id Original image ID (optional)
		 * @return int media ID
		 */
		public function guess_import_media($image_name, $image_src, $date='', $image_id=0) {
			// Optimization to get the right image filename
			$media_id = FALSE;
			if ( FALSE ) {
				$media_id = $this->import_media($image_name, $image_src[$this->image_filename_key], $date, array(), $image_id);
			}
			if ( $media_id === FALSE ) {
					if ( $image_src ) {
							$media_id = $this->import_media($image_name, $image_src, $date, array(), $image_id);
					}
			}
			return $media_id;
		}
		
		/**
		 * Returns the imported post ID corresponding to a meta key and value
		 *
		 * @since 3.3.0
		 * 
		 * @param string $meta_key Meta key
		 * @param string $meta_value Meta value
		 * @return int WordPress post ID
		 */
		public function get_wp_post_id_from_meta($meta_key, $meta_value) {
			global $wpdb;

			$sql = "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = '$meta_key' AND meta_value = '$meta_value' LIMIT 1";
			$post_id = $wpdb->get_var($sql);
			return $post_id;
		}		

		/**
		 * Import a media
		 *
		 * @param string $name Image name
		 * @param string $filename Image URL
		 * @param date $date Date (optional)
		 * @param array $options Options (optional)
		 * @param int $image_id Original image ID (optional)
		 * @return int attachment ID or FALSE
		 */
		public function import_media($name, $file_src, $date='', $options=array(), $image_id=0) {
			
			// Check if the media is already imported
			$attachment_id = $this->get_wp_post_id_from_meta('old_image_id', $image_id);
			
			if ( !$attachment_id ) {

				if ( empty($date) || ($date == '0000-00-00 00:00:00') ) {
					$date = date('Y-m-d H:i:s');
				}
				$import_external = true /* ($this->plugin_options['import_external'] == 1) || (isset($options['force_external']) && $options['force_external'] )*/;
				
				preg_match('@[^/?#]*\.[^/?#]*(\?.*)?(\#.*)?$@', $file_src, $filename);
				// print_r($filename[0]);
				$filename = urldecode($filename[0]);// for filenames with spaces or accents
				
				$filetype = wp_check_filetype($filename);
				if ( empty($filetype['type']) || ($filetype['type'] == 'text/html') ) { // Unrecognized file type
					echo 'error tipo ok';
					return FALSE;
				}

				
					$old_filename = $file_src;
				
				$old_filename = str_replace(" ", "%20", $old_filename); // for filenames with spaces

				// Get the upload path
				$upload_path = $this->upload_dir($filename, $date);

				// Make sure we have an uploads directory.
				if ( !wp_mkdir_p($upload_path) ) {
					return FALSE;
				}

				$new_filename = $filename;
				if ( true) {
					// Images with duplicate names
					$new_filename = preg_replace('#.*img/#', '', $new_filename);
					$new_filename = str_replace('http://', '', $new_filename);
					$new_filename = str_replace('/', '_', $new_filename);
				}

				$basename = basename($new_filename);
				$extension = substr(strrchr($basename, '.'), 1);
				$basename_without_extension = preg_replace('/(\.[^.]+)$/', '', $basename);
				$post_title = $name;
				$new_full_filename = $upload_path . '/' . $this->format_filename($name) . '.' . $extension;


				if ( ! @$this->remote_copy($old_filename, $new_full_filename) ) {
					echo 'ERROR REMOTE COPY';
					return FALSE;
				}

				// Image Alt
				$image_alt = '';
				if ( !empty($name) ) {
					$image_alt = wp_strip_all_tags(stripslashes($name), TRUE);
				}

				// GUID
				$upload_dir = wp_upload_dir();
				$guid = str_replace($upload_dir['basedir'], $upload_dir['baseurl'], $new_full_filename);
				$attachment_id = $this->insert_attachment($post_title, $basename, $new_full_filename, $guid, $date, $filetype['type'], $image_alt, $image_id);
			}
			return $attachment_id;
		}

		/**
		 * Determine the media upload directory
		 * 
		 * @since 2.3.0
		 * 
		 * @param string $filename Filename
		 * @param date $date Date
		 * @return string Upload directory
		 */
		public function upload_dir($filename, $date) {
			$upload_dir = wp_upload_dir(strftime('%Y/%m', strtotime($date)));
			$use_yearmonth_folders = get_option('uploads_use_yearmonth_folders');
			if ( $use_yearmonth_folders ) {
				$upload_path = $upload_dir['path'];
			} else {
				$short_filename = preg_replace('#.*img/#', '/', $filename);
				if ( strpos($short_filename, '/') != 0 ) {
					$short_filename = '/' . $short_filename; // Add a slash before the filename
				}
				$upload_path = $upload_dir['basedir'] . untrailingslashit(dirname($short_filename));
			}
			return $upload_path;
		}
		
		/**
		 * Format a filename
		 * 
		 * @since 3.7.3
		 * 
		 * @param string $filename Filename
		 * @return string Formated filename
		 */
		public function format_filename($filename) {
			$filename = $this->convert_to_latin($filename);
			$filename = preg_replace('/%.{2}/', '', $filename); // Remove the encoded characters
			$filename = sanitize_file_name($filename);
			return $filename;
		}		
		
		/**
		 * Convert string to latin
		 */
		public function convert_to_latin($string) {
			$string = self::greek_to_latin($string); // For Greek characters
			$string = self::cyrillic_to_latin($string); // For Cyrillic characters
			return $string;
		}
		
		/**
		 * Convert Greek characters to latin
		 */
		private static function greek_to_latin($string) {
			static $from = array('Α','Ά','Β','Γ','Δ','Ε','Έ','Ζ','Η','Θ','Ι','Κ','Λ','Μ','Ν','Ξ','Ο','Π','Ρ','Σ','Τ','Υ','Φ','Χ','Ψ','Ω','α','ά','β','γ','δ','ε','έ','ζ','η','ή','θ','ι','ί','ϊ','κ','λ','μ','ν','ξ','ο','ό','π','ρ','ς','σ','τ','υ','ύ','φ','χ','ψ','ω','ώ','ϑ','ϒ','ϖ');
			static $to = array('A','A','V','G','D','E','E','Z','I','TH','I','K','L','M','N','X','O','P','R','S','T','Y','F','CH','PS','O','a','a','v','g','d','e','e','z','i','i','th','i','i','i','k','l','m','n','x','o','o','p','r','s','s','t','y','y','f','ch','ps','o','o','th','y','p');
			return str_replace($from, $to, $string);
		}

		/**
		 * Convert Cyrillic (Russian) characters to latin
		 */
		private static function cyrillic_to_latin($string) {
			static $from = array('ж',  'ч',  'щ',   'ш',  'ю',  'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я', 'Ж',  'Ч',  'Щ',   'Ш',  'Ю',  'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я');
			static $to = array('zh', 'ch', 'sht', 'sh', 'yu', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'y', 'x', 'q', 'Zh', 'Ch', 'Sht', 'Sh', 'Yu', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'c', 'Y', 'X', 'Q');
			return str_replace($from, $to, $string);
		}
		
		/**
		 * Copy a remote file
		 * in replacement of the copy function
		 * 
		 * @param string $url URL of the source file
		 * @param string $path destination file
		 * @return boolean
		 */
		public function remote_copy($url, $path) {
			
			// Don't copy the file if already copied
			if ( file_exists($path) && (filesize($path) > 0) ) {
				return TRUE;
			}
			
			$response = wp_remote_get($url, array(
				'timeout'		=> $this->plugin_options['timeout'],
				'sslverify'		=> FALSE,
				'redirection'	=> 0,
			)); // Uses WordPress HTTP API
			
			if ( is_wp_error($response) ) {
				trigger_error($response->get_error_message(), E_USER_WARNING);
				return FALSE;
			} elseif ( $response['response']['code'] != 200 ) {
				trigger_error($response['response']['message'], E_USER_WARNING);
				return FALSE;
			} else {
				file_put_contents($path, wp_remote_retrieve_body($response));
				return TRUE;
			}
		}
		/**
		 * Save the attachment and generates its metadata
		 * 
		 * @since 2.3.0
		 * 
		 * @param string $attachment_title Attachment name
		 * @param string $basename Original attachment filename
		 * @param string $new_full_filename New attachment filename with path
		 * @param string $guid GUID
		 * @param date $date Date
		 * @param string $filetype File type
		 * @param string $image_alt Image description
		 * @param int $image_id Image ID
		 * @return int|FALSE Attachment ID or FALSE
		 */
		public function insert_attachment($attachment_title, $basename, $new_full_filename, $guid, $date, $filetype, $image_alt='', $image_id=0) {
			$post_name = sanitize_title($attachment_title);
			
			// If the attachment does not exist yet, insert it in the database
			$attachment_id = 0;
			$attachment = $this->get_attachment_from_name($post_name);
			if ( $attachment ) {
				$attached_file = basename(get_attached_file($attachment->ID));
				if ( $attached_file == $basename ) { // Check if the filename is the same (in case where the legend is not unique)
					$attachment_id = $attachment->ID;
				}
			}
			if ( $attachment_id == 0 ) {
				$attachment_data = array(
					'guid'				=> $guid, 
					'post_date'			=> $date,
					'post_mime_type'	=> $filetype,
					'post_name'			=> $post_name,
					'post_title'		=> $attachment_title,
					'post_status'		=> 'inherit',
					'post_content'		=> '',
				);
				$attachment_id = wp_insert_attachment($attachment_data, $new_full_filename);
				if ( !empty($image_id) ) {
					add_post_meta($attachment_id, 'old_image_id', $image_id, TRUE);
				} else {
					add_post_meta($attachment_id, '_imported', 1, TRUE); // To delete the imported attachments
				}
			}
			
			if ( !empty($attachment_id) ) {
				if ( preg_match('/image/', $filetype) ) { // Images
					// you must first include the image.php file
					// for the function wp_generate_attachment_metadata() to work
					require_once(ABSPATH . 'wp-admin/includes/image.php');
					$attach_data = wp_generate_attachment_metadata( $attachment_id, $new_full_filename );
					wp_update_attachment_metadata($attachment_id, $attach_data);

					// Image Alt
					if ( !empty($image_alt) ) {
						update_post_meta($attachment_id, '_wp_attachment_image_alt', addslashes($image_alt)); // update_post_meta expects slashed
					}
				}
				return $attachment_id;
			} else {
				return FALSE;
			}
		}
		
		/**
		 * Check if the attachment exists in the database
		 *
		 * @param string $name
		 * @return object Post
		 */
		private function get_attachment_from_name($name) {
			$name = preg_replace('/\.[^.]+$/', '', basename($name));
			$r = array(
				'name'			=> $name,
				'post_type'		=> 'attachment',
				'numberposts'	=> 1,
			);
			$posts_array = get_posts($r);
			if ( is_array($posts_array) && (count($posts_array) > 0) ) {
				return $posts_array[0];
			}
			else {
				return FALSE;
			}
		}
		
		/**
		 * Import post medias from content
		 *
		 * @param string $content post content
		 * @param date $post_date Post date (for storing media)
		 * @param array $options Options
		 * @return array:
		 * 		array media: Medias imported
		 * 		int media_count:   Medias count
		 */
		public function import_media_from_content($content, $post_date,  $url, $options=array()) {
			$media = array();
			$media_count = 0;
			$matches = array();
			$alt_matches = array();
			
			if ( preg_match_all('#<(img|a)(.*?)(src|href)="(.*?)"(.*?)>#', $content, $matches, PREG_SET_ORDER) > 0 ) {
				if ( is_array($matches) ) {
					foreach ($matches as $match ) {
						$filename = $match[4];
						$other_attributes = $match[2] . $match[5];
						// Image Alt
						$image_alt = '';
						if (preg_match('#alt="(.*?)"#', $other_attributes, $alt_matches) ) {
							$image_alt = wp_strip_all_tags(stripslashes($alt_matches[1]), TRUE);
						}
						$attach_id = $this->import_media($image_alt, $filename, $post_date, $options, $url);
						if ( $attach_id !== FALSE ) {
							$media_count++;
							$attachment = get_post($attach_id);
							if ( !is_null($attachment) ) {
								$media[$filename] = array(
									'id'	=> $attach_id,
									'name'	=> $attachment->post_name,
								);
							}
						}
					}
				}
			}
			return array(
				'media'			=> $media,
				'media_count'	=> $media_count
			);
		}

		/**
		 * Process the post content
		 *
		 * @param string $content Post content
		 * @param array $post_media Post medias
		 * @return string Processed post content
		 */
		public function process_content($content, $post_media) {
			
			if ( !empty($content) ) {
				$content = str_replace(array("\r", "\n"), array('', ' '), $content);
				
				// Replace page breaks
				$content = preg_replace("#<hr([^>]*?)class=\"system-pagebreak\"(.*?)/>#", "<!--nextpage-->", $content);
				
				// Replace media URLs with the new URLs
				$content = $this->process_content_media_links($content, $post_media);
			}

			return $content;
		}
		
		/**
		 * Replace media URLs with the new URLs
		 *
		 * @param string $content Post content
		 * @param array $post_media Post medias
		 * @return string Processed post content
		 */
		private function process_content_media_links($content, $post_media) {
			$matches = array();
			$matches_caption = array();
			
			if ( is_array($post_media) ) {
				
				// Get the attachments attributes
				$attachments_found = FALSE;
				foreach ( $post_media as $old_filename => &$media_var ) {
					$post_media_name = $media_var['name'];
					$attachment = $this->get_attachment_from_name($post_media_name);
					if ( $attachment ) {
						$media_var['attachment_id'] = $attachment->ID;
						$media_var['url_old_filename'] = urlencode($old_filename); // for filenames with spaces
						if ( preg_match('/image/', $attachment->post_mime_type) ) {
							// Image
							$image_src = wp_get_attachment_image_src($attachment->ID, 'full');
							$media_var['new_url'] = $image_src[0];
							$media_var['width'] = $image_src[1];
							$media_var['height'] = $image_src[2];
						} else {
							// Other media
							$media_var['new_url'] = wp_get_attachment_url($attachment->ID);
						}
						$attachments_found = TRUE;
					}
				}
				if ( $attachments_found ) {
				
					// Remove the links from the content
					$this->post_link_count = 0;
					$this->post_link = array();
					$content = preg_replace_callback('#<(a) (.*?)(href)=(.*?)</a>#i', array($this, 'remove_links'), $content);
					$content = preg_replace_callback('#<(img) (.*?)(src)=(.*?)>#i', array($this, 'remove_links'), $content);
					
					// Process the stored medias links
					$first_image_removed = false;
					foreach ($this->post_link as &$link) {
						
						// Remove the first image from the content
						if ( !$first_image_removed && preg_match('#^<img#', $link['old_link']) ) {
							$link['new_link'] = '';
							$first_image_removed = TRUE;
							continue;
						}
						$new_link = $link['old_link'];
						$alignment = '';
						if ( preg_match('/(align="|float: )(left|right)/', $new_link, $matches) ) {
							$alignment = 'align' . $matches[2];
						}
						if ( preg_match_all('#(src|href)="(.*?)"#i', $new_link, $matches, PREG_SET_ORDER) ) {
							$caption = '';
							foreach ( $matches as $match ) {
								$old_filename = $match[2];
								$link_type = ($match[1] == 'src')? 'img': 'a';
								if ( array_key_exists($old_filename, $post_media) ) {
									$media = $post_media[$old_filename];
									if ( array_key_exists('new_url', $media) ) {
										if ( (strpos($new_link, $old_filename) > 0) || (strpos($new_link, $media['url_old_filename']) > 0) ) {
											$new_link = preg_replace('#('.$old_filename.'|'.$media['url_old_filename'].')#', $media['new_url'], $new_link, 1);
											
											if ( $link_type == 'img' ) { // images only
												// Define the width and the height of the image if it isn't defined yet
												if ((strpos($new_link, 'width=') === FALSE) && (strpos($new_link, 'height=') === FALSE)) {
													$width_assertion = isset($media['width'])? ' width="' . $media['width'] . '"' : '';
													$height_assertion = isset($media['height'])? ' height="' . $media['height'] . '"' : '';
												} else {
													$width_assertion = '';
													$height_assertion = '';
												}
												
												// Caption shortcode
												if ( preg_match('/class=".*caption.*?"/', $link['old_link']) ) {
													if ( preg_match('/title="(.*?)"/', $link['old_link'], $matches_caption) ) {
														$caption_value = str_replace('%', '%%', $matches_caption[1]);
														$align_value = ($alignment != '')? $alignment : 'alignnone';
														$caption = '[caption id="attachment_' . $media['attachment_id'] . '" align="' . $align_value . '"' . $width_assertion . ']%s' . $caption_value . '[/caption]';
													}
												}
												
												$align_class = ($alignment != '')? $alignment . ' ' : '';
												$new_link = preg_replace('#<img(.*?)( class="(.*?)")?(.*) />#', "<img$1 class=\"$3 " . $align_class . 'size-full wp-image-' . $media['attachment_id'] . "\"$4" . $width_assertion . $height_assertion . ' />', $new_link);
											}
										}
									}
								}
							}
							
							// Add the caption
							if ( $caption != '' ) {
								$new_link = sprintf($caption, $new_link);
							}
						}
						$link['new_link'] = $new_link;
					}
					
					// Reinsert the converted medias links
					$content = preg_replace_callback('#__fg_link_(\d+)__#', array($this, 'restore_links'), $content);
				}
			}
			return $content;
		}
		
		/**
		 * Remove all the links from the content and replace them with a specific tag
		 * 
		 * @param array $matches Result of the preg_match
		 * @return string Replacement
		 */
		private function remove_links($matches) {
			$this->post_link[] = array('old_link' => $matches[0]);
			return '__fg_link_' . $this->post_link_count++ . '__';
		}
		
		/**
		 * Get the IDs of the medias
		 *
		 * @param array $post_media Post medias
		 * @return array Array of attachment IDs
		 */
		public function get_attachment_ids($post_media) {
			$attachments_ids = array();
			if ( is_array($post_media) ) {
				foreach ( $post_media as $media ) {
					$attachment = $this->get_attachment_from_name($media['name']);
					if ( !empty($attachment) ) {
						$attachments_ids[] = $attachment->ID;
					}
				}
			}
			return $attachments_ids;
		}
		
		/**
		 * Add a link between a media and a post (parent id + thumbnail)
		 *
		 * @param int $post_id Post ID
		 * @param array $post_media Post medias
		 * @param array $date Date
		 * @param boolean $set_featured_image Set the featured image?
		 */
		public function add_post_media($post_id, $post_media, $date, $set_featured_image=TRUE) {
			$thumbnail_is_set = FALSE;
			if ( is_array($post_media) ) {
				foreach ( $post_media as $media ) {
					$attachment = get_post($media);
					if ( !empty($attachment) && ($attachment->post_type == 'attachment') ) {
						$attachment->post_parent = $post_id; // Attach the post to the media
						$attachment->post_date = $date ;// Define the media's date
						wp_update_post($attachment);

						// Set the featured image. If not defined, it is the first image of the content.
						if ( $set_featured_image && !$thumbnail_is_set ) {
							set_post_thumbnail($post_id, $attachment->ID);
							$thumbnail_is_set = TRUE;
						}
					}
				}
			}
		}		
		
		
		/**
		 * Recalculate the terms counters
		 * 
		 */
		private function recount_terms() {
			$taxonomy_names = wc_get_attribute_taxonomy_names();
			foreach ( $taxonomy_names as $taxonomy ) {
				$terms = get_terms($taxonomy, array('hide_empty' => 0));
				$termtax = array();
				foreach ( $terms as $term ) {
					$termtax[] = $term->term_taxonomy_id; 
				}
				wp_update_term_count($termtax, $taxonomy);
			}
		}
		
		/**
	 * Helper method to get coupon post objects
	 *
	 * @since 2.1
	 * @param array $args request arguments for filtering query
	 * @return WP_Query
	 */
		private function get_query_coupons(  ) {
	
			$args = array(
			    'posts_per_page'   => -1,
			    'orderby'          => 'title',
			    'order'            => 'asc',
			    'post_type'        => 'shop_coupon',
			    'post_status'      => 'publish',
			);
		    
			return get_posts( $args );
		}
		
		/**
		 * Chequear si ya existe el mismo sku
		 * 
		 */
		 public function get_product_by_sku( $sku ) {

			global $wpdb;
			
			$product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku ) );
		
			if ( $product_id ) return new WC_Product( $product_id );
		
			return null;
			
		}
		
		public function img_count_media(){
                $query_img_args = array(
                        'post_type' => 'attachment',
                        'post_mime_type' =>array(
                                        'jpg|jpeg|jpe' => 'image/jpeg',
                                        'gif' => 'image/gif',
                                        'png' => 'image/png',
                                        ),
                        'post_status' => 'inherit',
                        'posts_per_page' => -1,
                        );
                $query_img = new WP_Query( $query_img_args );
                return intval($query_img->post_count);
        }
   //TERMINAN FUNCIONES PARA IMPORTAR PRODUCTOS
   
   
	// hide coupon field on cart page
	public function hide_coupon_field_on_cart( $enabled ) {
		if ( is_cart() || is_checkout()) {
			$enabled = false;
		}
		return $enabled;
	}
	
    
    /**
	 * Create a customer
	 *
	 * @since 2.2
	 *
	 * @param array $data
	 *
	 * @return array|WP_Error
	 */
	private function create_customer_wcs( $data, $host_id ) {
		try {
			if ( ! isset( $data['customer'] ) ) {
				return new WP_Error('Objeto incorrecto!');
			}

			$data = $data['customer'];

			// Checks with the email is missing.
			if ( ! isset( $data['email'] ) ) {
				return new WP_Error('No se encuentra el email');
			}

			// Create customer.
			$customer = new WC_Customer;
			$customer->set_username( ! empty( $data['username'] ) && ! username_exists( $data['username'] ) ? $data['username'] : '' );
			$customer->set_password(wp_generate_password());
			$customer->set_email( $data['email'] );
			$customer->save();

			if ( ! $customer->get_id() ) {
				return new WP_Error('No se puedo crear el ID');
			}

			// Added customer data.
			$this->update_customer_data( $customer->get_id(), $data, $customer );
			$customer->save();

			if($host_id!='' && !empty($host_id)){add_user_meta($customer->get_id(), 'wcs_shop_server_tienda_cliente', $host_id, true);}

			return $customer->get_id();
		} catch ( Exception $e ) {
			return new WP_Error( $e->getErrorCode(), $e->getMessage(), array( 'status' => $e->getCode() ) );
		}
	} 
	
		/**
	 * Add/Update customer data.
	 *
	 * @since 2.2
	 * @param int $id the customer ID
	 * @param array $data
	 * @param WC_Customer $customer
	 */
	protected function update_customer_data( $id, $data, $customer ) {

		// Customer first name.
		if ( isset( $data['first_name'] ) ) {
			$customer->set_first_name( wc_clean( $data['first_name'] ) );
		}

		// Customer last name.
		if ( isset( $data['last_name'] ) ) {
			$customer->set_last_name( wc_clean( $data['last_name'] ) );
		}

		// Customer billing address.
		if ( isset( $data['billing_address'] ) ) {
			foreach ( $this->get_customer_billing_address() as $field ) {
				if ( isset( $data['billing_address'][ $field ] ) ) {
					if ( is_callable( array( $customer, "set_billing_{$field}" ) ) ) {
						$customer->{"set_billing_{$field}"}( $data['billing_address'][ $field ] );
					} else {
						$customer->update_meta_data( 'billing_' . $field, wc_clean( $data['billing_address'][ $field ] ) );
					}
				}
			}
		}

		// Customer shipping address.
		if ( isset( $data['shipping_address'] ) ) {
			foreach ( $this->get_customer_shipping_address() as $field ) {
				if ( isset( $data['shipping_address'][ $field ] ) ) {
					if ( is_callable( array( $customer, "set_shipping_{$field}" ) ) ) {
						$customer->{"set_shipping_{$field}"}( $data['shipping_address'][ $field ] );
					} else {
						$customer->update_meta_data( 'shipping_' . $field, wc_clean( $data['shipping_address'][ $field ] ) );
					}
				}
			}
		}

	}
	
		/**
	 * Get customer billing address fields.
	 *
	 * @since  2.2
	 * @return array
	 */
	protected function get_customer_billing_address() {
		$billing_address = apply_filters( 'woocommerce_api_customer_billing_address', array(
			'first_name',
			'last_name',
			'company',
			'address_1',
			'address_2',
			'city',
			'state',
			'postcode',
			'country',
			'email',
			'phone',
		) );

		return $billing_address;
	}

	/**
	 * Get customer shipping address fields.
	 *
	 * @since  2.2
	 * @return array
	 */
	protected function get_customer_shipping_address() {
		$shipping_address = apply_filters( 'woocommerce_api_customer_shipping_address', array(
			'first_name',
			'last_name',
			'company',
			'address_1',
			'address_2',
			'city',
			'state',
			'postcode',
			'country',
		) );

		return $shipping_address;
	}

    public function wcs_shop_server_admin_order_meta($order){
    	  
    	  $tienda_cliente = array();
			
			// add line items falsos pasa el id servidor!
			foreach( $order->get_items() as $item_id => $item ) {
				$product    = $item->get_product();
				$tienda_cliente[] = get_post_meta($product->id, 'wcs_shop_server__tienda_vitrina',  TRUE);
			}
			
			if(count($tienda_cliente) < 1 && empty($tienda_cliente[0])){
    		 	unset($tienda_cliente);
    		 	return;
	 		 }
		
		  $vinculo_tienda_cliente = $tienda_cliente[0]['url-shop'];
        $tienda_cliente = $tienda_cliente[0]['host'];
        if($tienda_cliente !=''){ echo '<p style="font-size: 13px;color: #777;margin: 20px 0;" ><strong style=" display: block; " >'.__('Tienda Cliente').': </strong> <a href="'.$vinculo_tienda_cliente.'" target="_blank">'.$tienda_cliente. '</a></p>';}
    
    }
    
    public function funciones_de_incio(){
    	remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
    }
    
    public function custom_cart(){
    	if(is_cart()){
    		 $tienda_cliente = array();
    		 foreach(WC()->cart->get_cart() as $item => $value){
    		 	$tienda_cliente[] = get_post_meta($value['product_id'], 'wcs_shop_server__tienda_vitrina',  TRUE);
    		 }
    		 if(count($tienda_cliente) < 1 && empty($tienda_cliente[0])){
    		 	unset($tienda_cliente);
    		 	return;
	 		 }
	 		 $url_tienda =$tienda_cliente[0]['url-shop'];
	 		 unset($tienda_cliente);
    		?>
    			<script type="text/javascript">
    				 jQuery(function(){
	                jQuery('body').find('td.product-remove > a.remove').remove();
	                jQuery('body').find('input.input-text.qty').attr('readonly', 'readonly');
	                var padre = jQuery('body').find('input[name="update_cart"]').parent().get( 0 );
	                jQuery('body').find('input[name="update_cart"]').remove();
	                var boton = "<a type='button' class='button' href='"+document.referrer+"'>Actualizar carrito</a>";
	                var boton2 = "<a type='button' class='button' href='<?php echo $url_tienda; ?>'>Seguir comprando</a>";
	                jQuery(padre).prepend(boton2).append(boton);
	            });
    			</script>
    		<?php
    	}
    }
    
   public function orden_cambia_estado($order_id){
   	$order = wc_get_order($order_id);
   	if ( $order ) {
		  	 $tienda_cliente = array();
	 		 foreach( $order->get_items() as $item_id => $item ) {
				$product    = $item->get_product();
				$tienda_cliente[] = get_post_meta($product->id, 'wcs_shop_server__tienda_vitrina',  TRUE);
			 }
	 		 if(count($tienda_cliente) < 1 && empty($tienda_cliente[0])){
	 		 	unset($tienda_cliente);
	 		 	return;
	 		 }
   	}
   	
   	$estados = array( 'failed_order' => 'failed', 'cancelled_order' => 'cancelled','customer_processing_order' => 'processing' , 'customer_completed_order' => 'completed', 'customer_refunded_order' => 'refunded');
   	$data = array('order' => array('status' => $order->get_status(), 'payment_details' => array('method_id' => $order->get_payment_method(),'method_title' => $order->get_payment_method_title(),'paid' => $new_status === 'completed' ? true : ! is_null( $order->get_date_paid() ),) ));// $new_status 
   	
   	$vitrinas = wcs_shop_server_deserializer::getInstance()->get_value('opciones_clientes');
	   $key_v = $this->busquedaURLopciones($tienda_cliente[0]['host'], $vitrinas);
 	   $vitrina_sl= array_keys($vitrinas[$key_v]);
 	   $client_api = apply_filters('wcs_shop_server_api_action', $vitrinas[$key_v][$vitrina_sl[2]], $vitrinas[$key_v][$vitrina_sl[4]], $vitrinas[$key_v][$vitrina_sl[3]]); 
 	   if(!($client_api === 'ERROR')){
			try {
			   if(strval($order->get_status()) != 'pending'){
    			    if($vitrinas[$key_v][$vitrina_sl[5]]==='checked'){
    					if ( ! empty( WC()->mailer()->get_emails() ) ) {
    				        foreach ( WC()->mailer()->get_emails() as $mail ) {
    				            if ( $mail->id == strval(array_search($order->get_status(),$estados)) ) {
    				        	    $mail->enabled = 'no';
    				            }
    				        }
    				    }
    			     }
			      }
			    $order_update = $client_api->orders->update(intval(esc_attr(get_post_meta($order_id,'_cliente_1_order_id' ,true))), $data);
			} catch (WC_API_Client_Exception $e) {
			    sprintf( __('OCURRIO UN ERROR EN LA CONEXION CON LA TIENDA CLIENTE. ' ), 'error' );
			}
 	   }
   }
    
   public function reload_boton_thankyou(){
    	global $wp;
		
	   if ( is_order_received_page() ) {
	   	$order_id = wc_get_order_id_by_order_key($_GET['key']);
      	$order = wc_get_order($order_id);
	   	  if ( $order ) {
	   	  	 $tienda_cliente = array();
	    		 foreach( $order->get_items() as $item_id => $item ) {
					$product    = $item->get_product();
					$tienda_cliente[] = get_post_meta($product->id, 'wcs_shop_server__tienda_vitrina',  TRUE);
				 }
	    		 if(count($tienda_cliente) < 1 && empty($tienda_cliente[0])){
	    		 	unset($tienda_cliente);
	    		 	return;
		 		 }
		 		 $url_tienda = esc_url($tienda_cliente[0]['url-shop']);
		 		 unset($tienda_cliente);
		 		?>
				<script type="text/javascript" >
					jQuery('section.woocommerce-order-details').append('<a style="margin: 15px auto;display: block;text-align: center;" href="<?php echo $url_tienda ?>" ><input value="Volver a la Tienda" style="font-size: 1.5em;border-radius: 6px;" type="button"></a>');
				</script>
				<?php
	   	 }
		}
    }
    
    /**
	 * simple method to encrypt or decrypt a plain text string
	 * initialization vector(IV) has to be the same when encrypting and decrypting
	 * 
	 * @param string $action: can be 'encrypt' or 'decrypt'
	 * @param string $string: string to encrypt or decrypt
	 * @param string $md5_string: string key verified
	 *
	 * @return string
	*/
	private function encrypt_decrypt($action, $string, $md5_string) {
	    $output = false;
	    
	   $dirty = array("+", "/", "=");
		$clean = array("_PIOPIO_", "_SAPOAPO_", "_ECOECO_");
	    
	    if ( $action == 'encrypt' ) {
	    	$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
		    $secret_iv = mcrypt_create_iv($iv_size, $this->md5_hex_to_dec($md5_string));
		    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $md5_string, utf8_encode($string), MCRYPT_MODE_ECB, $secret_iv );
		    $encrypted_string = base64_encode($encrypted_string);
	    	
	        $output = str_replace($dirty, $clean, $encrypted_string);
	    } else if( $action == 'decrypt' ) {
	    	$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
		    $secret_iv = mcrypt_create_iv($iv_size, $this->md5_hex_to_dec($md5_string));
		    
		    $string = base64_decode(str_replace($clean, $dirty, $string));
		    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $md5_string, $string, MCRYPT_MODE_ECB, $secret_iv);
		    
	        $output = $decrypted_string;
	    }
	    return $output;
	}
	
	
	private function md5_hex_to_dec($hex_str){
	    $arr = str_split($hex_str, 4);
	    foreach ($arr as $grp) {
	        $dec[] = str_pad(hexdec($grp), 5, '0', STR_PAD_LEFT);
	    }
	    return implode('', $dec);
	}
	
	
	//FUNCION PARA REVISAR LAS CLASES DE IMPUESTOS
	public function clase_host_id( $tax_class, $product ) {  
		 
		 // Get tax classes and display as links
		 $tax_classes = WC_Tax::get_tax_classes();
		 $tienda_cliente = get_post_meta($product->id, 'wcs_shop_server__tienda_vitrina',  TRUE);
		 if( !empty($tienda_cliente['host']) && isset($tienda_cliente['host']) && in_array($tienda_cliente['host'], $tax_classes)){
		 	$tax_class = $tienda_cliente['host'];
		 }
		 
		 return $tax_class;  
	}
}
