<?php

/**
 * The menus functionality of the plugin.
 *
 * @link       http://example.com
 * @since      0.0.1
 *
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/admin/menus
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/admin/menus
 * @author     Your Name <email@example.com>
 */
 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 if ( ! class_exists( 'wcs_shop_server_submenu' ) ) :

include_once( plugin_dir_path( __FILE__ ) . 'class-submenu-page.php' );
 
 class wcs_shop_server_submenu{
 
   	 
	 public function __construct( ) {
		add_action('admin_menu', array(__CLASS__,'wcts_submenu_'),99);
	}

	public static function wcts_submenu_() {
            $clase =wcs_shop_server_submenu_page::getInstance();
            $mainpage = add_submenu_page( 'woocommerce', 'WCS Clientes', 'WCS Clientes', 'manage_options', 'wcs_shop_server_opciones', array($clase,'submenu_page_') ); 
    }

     
 }
 endif;
 
 return new wcs_shop_server_submenu();
