<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/public
 * @author     Your Name <email@example.com>
 */
class wcs_shop_server_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.0.1
	 * @access   private
	 * @var      string    $wcs_shop_server    The ID of this plugin.
	 */
	private $wcs_shop_server;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.0.1
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.0.1
	 * @param      string    $wcs_shop_server       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $wcs_shop_server, $version ) {

		$this->wcs_shop_server = $wcs_shop_server;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    0.0.1
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in wcs_shop_server_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The wcs_shop_server_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->wcs_shop_server, plugin_dir_url( __FILE__ ) . 'css/wcs-shop-server-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    0.0.1
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in wcs_shop_server_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The wcs_shop_server_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->wcs_shop_server, plugin_dir_url( __FILE__ ) . 'js/wcs-shop-server-public.js', array( 'jquery' ), $this->version, false );

	}

}
