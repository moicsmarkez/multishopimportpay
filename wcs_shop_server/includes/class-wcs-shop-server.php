<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      0.0.1
 *
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      0.0.1
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/includes
 * @author     Your Name <email@example.com>
 */
 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit

if ( ! class_exists( 'wcs_shop_server' ) ) :
	
	class wcs_shop_server {
	
		/**
		 * The loader that's responsible for maintaining and registering all hooks that power
		 * the plugin.
		 *
		 * @since    0.0.1
		 * @access   protected
		 * @var      wcs_shop_server_Loader    $loader    Maintains and registers all hooks for the plugin.
		 */
		protected $loader;
	
		/**
		 * The unique identifier of this plugin.
		 *
		 * @since    0.0.1
		 * @access   protected
		 * @var      string    $wcs_shop_server    The string used to uniquely identify this plugin.
		 */
		protected $wcs_shop_server;
	
		/**
		 * The current version of the plugin.
		 *
		 * @since    0.0.1
		 * @access   protected
		 * @var      string    $version    The current version of the plugin.
		 */
		protected $version;
	
		/**
		 * Define the core functionality of the plugin.
		 *
		 * Set the plugin name and the plugin version that can be used throughout the plugin.
		 * Load the dependencies, define the locale, and set the hooks for the admin area and
		 * the public-facing side of the site.
		 *
		 * @since    0.0.1
		 */
		public function __construct() {
	
			$this->wcs_shop_server = 'wcs-shop-server';
			$this->version = '0.0.1';
	
			$this->load_dependencies();
			//$this->set_locale(); ESTOS SE DEBEN COLOCAR DESPUES
			$this->define_admin_hooks();
			//$this->define_public_hooks(); IGUAL QUE AQUI HAY QUE COLOCARLOS DESPUES
	
		}
	
		/**
		 * Load the required dependencies for this plugin.
		 *
		 * Include the following files that make up the plugin:
		 *
		 * - wcs_shop_server_Loader. Orchestrates the hooks of the plugin.
		 * - wcs_shop_server_i18n. Defines internationalization functionality.
		 * - wcs_shop_server_Admin. Defines all hooks for the admin area.
		 * - wcs_shop_server_Public. Defines all hooks for the public side of the site.
		 *
		 * Create an instance of the loader which will be used to register the hooks
		 * with WordPress.
		 *
		 * @since    0.0.1
		 * @access   private
		 */
		private function load_dependencies() {
	
			/**
			 * The class responsible for orchestrating the actions and filters of the
			 * core plugin.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wcs-shop-server-loader.php';
	
			/**
			 * The class responsible for defining internationalization functionality
			 * of the plugin.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wcs-shop-server-i18n.php';
			
			/**
			 * Clase responsable para el manejo del api dentro 
			 * del plugin.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-api-rest.php';
	
			/**
			 * The class responsible for defining all actions that occur in the admin area.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wcs-shop-server-admin.php';
	
			/**
			 * Clase para incorporar nuevos endpoints en el api de woocom.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/woocommerce-extend-api.php';
			
			/**
			 * The class responsible for defining all actions that occur in the admin area.
			 */
			// require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/views/product-shops.php';
	
			/**
			 * The class responsible for defining all actions that occur in the public-facing
			 * side of the site.
			 */
		//	require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wcs-shop-server-public.php';
	
			$this->loader = new wcs_shop_server_Loader();
	
		}
	
		/**
		 * Define the locale for this plugin for internationalization.
		 *
		 * Uses the wcs_shop_server_i18n class in order to set the domain and to register the hook
		 * with WordPress.
		 *
		 * @since    0.0.1
		 * @access   private
		 */
		private function set_locale() {
	
			$plugin_i18n = new wcs_shop_server_i18n();
	
			$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'wcs-shop-server' );
	
		}
	
		/**
		 * Register all of the hooks related to the admin area functionality
		 * of the plugin.
		 *
		 * @since    0.0.1 
		 * @access   private
		 */
		private function define_admin_hooks() {
	
			$plugin_admin = new wcs_shop_server_Admin( $this->get_wcs_shop_server(), $this->get_version() );
			// $settings_products = new wcs_shop_server_settings_product();
	
			$this->loader->add_action( 'init', $plugin_admin, 'funciones_de_incio', 20);
	    	$this->loader->add_action( 'template_redirect',$plugin_admin, 'capturar_carrito', 20 ); 
			$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
			$this->loader->add_action( 'woocommerce_before_cart_contents', $plugin_admin, 'custom_cart', 20);
			$this->loader->add_action( 'wp_footer', $plugin_admin, 'reload_boton_thankyou', 20);
			$this->loader->add_action( 'woocommerce_thankyou', $plugin_admin, 'verifique_gracias');
			$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
			$this->loader->add_filter( 'woocommerce_product_get_tax_class', $plugin_admin, 'clase_host_id', 1, 2);
			$this->loader->add_filter( 'woocommerce_product_variation_get_tax_class', $plugin_admin, 'clase_host_id', 1, 2);
			$this->loader->add_filter( 'wcs_shop_server_api_action', wcs_shop_server_api::getInstance(), 'empezar_api', 20, 3);
			$this->loader->add_filter( 'woocommerce_api_product_response', $plugin_admin, 'get_tienda_tag', 20, 4);
			$this->loader->add_filter( 'woocommerce_coupons_enabled', $plugin_admin, 'hide_coupon_field_on_cart' );
			$this->loader->add_action( 'woocommerce_checkout_order_processed', $plugin_admin, 'actualiza_orden_nueva',  1, 1);
			$this->loader->add_action( 'woocommerce_admin_order_data_after_billing_address', $plugin_admin, 'wcs_shop_server_admin_order_meta', 10, 1 );
			
    	   	$estados = array(0 => 'pending', 'customer_on_hold_order' => 'on-hold', 'failed_order' => 'failed', 'cancelled_order' => 'cancelled','customer_processing_order' => 'processing' , 'customer_completed_order' => 'completed', 'customer_refunded_order' => 'refunded');
			foreach($estados as $key => $value){
			    $this->loader->add_action( 'woocommerce_order_status_'.$value, $plugin_admin, 'orden_cambia_estado', 10, 1);    
			}
			
			//$this->loader->add_filter( 'woocommerce_email_recipient_new_order', $plugin_admin, 'wc_change_admin_new_order_email_recipient', 1, 2);
			//$this->loader->add_filter( 'woocommerce_email_recipient_processing_order', $plugin_admin, 'wc_change_admin_new_order_email_recipient', 1, 2);
			

		}
	
		/**
		 * Register all of the hooks related to the public-facing functionality
		 * of the plugin.
		 *
		 * @since    0.0.1
		 * @access   private
		 */
		private function define_public_hooks() {
	
			$plugin_public = new wcs_shop_server_Public( $this->get_wcs_shop_server(), $this->get_version() );
	
			$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
			$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
	
		}
	
		/**
		 * Run the loader to execute all of the hooks with WordPress.
		 *
		 * @since    0.0.1
		 */
		public function run() {
			$this->loader->run();
		}
	
		/**
		 * The name of the plugin used to uniquely identify it within the context of
		 * WordPress and to define internationalization functionality.
		 *
		 * @since     0.0.1
		 * @return    string    The name of the plugin.
		 */
		public function get_wcs_shop_server() {
			return $this->wcs_shop_server;
		}
	
		/**
		 * The reference to the class that orchestrates the hooks with the plugin.
		 *
		 * @since     0.0.1
		 * @return    wcs_shop_server_Loader    Orchestrates the hooks of the plugin.
		 */
		public function get_loader() {
			return $this->loader;
		}
	
		/**
		 * Retrieve the version number of the plugin.
		 *
		 * @since     0.0.1
		 * @return    string    The version number of the plugin.
		 */
		public function get_version() {
			return $this->version;
		}
	
	}
  
endif;

return new wcs_shop_server();