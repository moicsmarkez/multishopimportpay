<?php


/**
 * Custom API REST path  class
 *
 * @package MyPlugin
 * @author Skatox
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
 
class WC_API_wcs_shop_server extends WC_API_Resource
{
    const PATH = '/tienda';
    /**
     * Function to define each of the custom path
     */
    public function register_routes($routes)
    {
      
        //GET Request
        $routes[self::PATH . '/vitrina/(?P<seudonimo_t>\w[\w\s\-\_]*)'] = array(
            array(array($this, 'get_vitrina_products'), WC_API_Server::READABLE),
            
        );
        
        //GET Request
        $routes[self::PATH . '/product/(?P<wcs_shop_server_id>\d+)'] = array(
            array(array($this, 'get_product_id_wcs_shop_server'), WC_API_Server::READABLE),
            
        );
        
         //GET Request
        $routes[self::PATH . '/producto_nuevo/(?P<id_cliente_in>\d+)'] = array(
            array(array($this, 'import_product_by_id_clientes_1'), WC_API_Server::READABLE),
        );
        
      
        return $routes;
    }
    
	/**
	 * Get all products
	 *
	 * @since 2.1
	 * @param string $fields
	 * @param string $type
	 * @param array $filter
	 * @param int $page
	 * @return array
	 */
	public function get_products( $fields = null, $type = null, $filter = array(), $page = 1 ) {

		if ( ! empty( $type ) ) {
			$filter['type'] = $type;
		}

		$filter['page'] = $page;

		$query = $this->query_products( $filter );

		$products = array();

		foreach ( $query->posts as $product_id ) {

			if ( ! $this->is_readable( $product_id ) ) {
				continue;
			}

			$products[] = current( $this->get_product( $product_id, $fields ) );
		}

		$this->server->add_pagination_headers( $query );

		return array( 'products' => $products );
	}
	
	/**
	 * Helper method to get product post objects
	 *
	 * @since 2.1
	 * @param array $args request arguments for filtering query
	 * @return WP_Query
	 */
	private function query_products( $args ) {

		// Set base query arguments
		$query_args = array(
			'fields'      => 'ids',
			'post_type'   => 'product',
			'post_status' => 'publish',
			'meta_query'  => array(),
		);

		if ( ! empty( $args['type'] ) ) {

			$types = explode( ',', $args['type'] );

			$query_args['tax_query'] = array(
				array(
					'taxonomy' => 'product_type',
					'field'    => 'slug',
					'terms'    => $types,
				),
			);

			unset( $args['type'] );
		}

		// Filter products by category
		if ( ! empty( $args['category'] ) ) {
			$query_args['product_cat'] = $args['category'];
		}

		// Filter by specific sku
		if ( ! empty( $args['sku'] ) ) {
			if ( ! is_array( $query_args['meta_query'] ) ) {
				$query_args['meta_query'] = array();
			}

			$query_args['meta_query'][] = array(
				'key'     => '_sku',
				'value'   => $args['sku'],
				'compare' => '=',
			);

			$query_args['post_type'] = array( 'product', 'product_variation' );
		}
		// Filter by specific tienda_vitrina
		if ( ! empty( $args['tienda_vitrina'] ) ) {
			if ( ! is_array( $query_args['meta_query'] ) ) {
				$query_args['meta_query'] = array();
			}

			$query_args['meta_query'][] = array(
				'key'     => 'wcs_shop_server_tienda_vitrina',
				'value'   => $args['tienda_vitrina'],
				'compare' => '=',
			);

			$query_args['post_type'] = array( 'product', 'product_variation' );
		}

		$query_args = $this->merge_query_args( $query_args, $args );

		return new WP_Query( $query_args );
	}
    
	/**
	 * Get the product for the given ID
	 *
	 * @since 2.1
	 * @param int $id the product ID
	 * @param string $fields
	 * @return array|WP_Error
	 */
	public function get_product( $id, $fields = null ) {

		$id = $this->validate_request( $id, 'product', 'read' );

		if ( is_wp_error( $id ) ) {
			return $id;
		}

		$product = wc_get_product( $id );

		// add data that applies to every product type
		$product_data = $this->get_product_data( $product );

		// add variations to variable products
		if ( $product->is_type( 'variable' ) && $product->has_child() ) {
			$product_data['variations'] = $this->get_variation_data( $product );
		}

		// add the parent product data to an individual variation
		if ( $product->is_type( 'variation' ) && $product->get_parent_id() ) {
			$_product = wc_get_product( $product->get_parent_id() );
			$product_data['parent'] = $this->get_product_data( $_product );
		}

		return array( 'product' => apply_filters( 'woocommerce_api_product_response', $product_data, $product, $fields, $this->server ) );
	}
	
	/**
	 * Get an individual variation's data
	 *
	 * @since 2.1
	 * @param WC_Product $product
	 * @return array
	 */
	private function get_variation_data( $product ) {
		$prices_precision = wc_get_price_decimals();
		$variations       = array();

		foreach ( $product->get_children() as $child_id ) {

			$variation = wc_get_product( $child_id );

			if ( ! $variation || ! $variation->exists() ) {
				continue;
			}

			$variations[] = array(
				'id'                => $variation->get_id(),
				'created_at'        => $this->server->format_datetime( $variation->get_date_created(), false, true ),
				'updated_at'        => $this->server->format_datetime( $variation->get_date_modified(), false, true ),
				'downloadable'      => $variation->is_downloadable(),
				'virtual'           => $variation->is_virtual(),
				'permalink'         => $variation->get_permalink(),
				'sku'               => $variation->get_sku(),
				'price'             => wc_format_decimal( $variation->get_price(), $prices_precision ),
				'regular_price'     => wc_format_decimal( $variation->get_regular_price(), $prices_precision ),
				'sale_price'        => $variation->get_sale_price() ? wc_format_decimal( $variation->get_sale_price(), $prices_precision ) : null,
				'taxable'           => $variation->is_taxable(),
				'tax_status'        => $variation->get_tax_status(),
				'tax_class'         => $variation->get_tax_class(),
				'managing_stock'    => $variation->managing_stock(),
				'stock_quantity'    => (int) $variation->get_stock_quantity(),
				'in_stock'          => $variation->is_in_stock(),
				'backordered'       => $variation->is_on_backorder(),
				'purchaseable'      => $variation->is_purchasable(),
				'visible'           => $variation->variation_is_visible(),
				'on_sale'           => $variation->is_on_sale(),
				'weight'            => $variation->get_weight() ? wc_format_decimal( $variation->get_weight(), 2 ) : null,
				'dimensions'        => array(
					'length' => $variation->get_length(),
					'width'  => $variation->get_width(),
					'height' => $variation->get_height(),
					'unit'   => get_option( 'woocommerce_dimension_unit' ),
				),
				'shipping_class'    => $variation->get_shipping_class(),
				'shipping_class_id' => ( 0 !== $variation->get_shipping_class_id() ) ? $variation->get_shipping_class_id() : null,
				'image'             => $this->get_images( $variation ),
				'attributes'        => $this->get_attributes( $variation ),
				'downloads'         => $this->get_downloads( $variation ),
				'download_limit'    => (int) $product->get_download_limit(),
				'download_expiry'   => (int) $product->get_download_expiry(),
			);
		}

		return $variations;
	}
	
    
	/**
	 * Get standard product data that applies to every product type
	 *
	 * @since 2.1
	 * @param WC_Product|int $product
	 * @return array
	 */
	private function get_product_data( $product ) {
		if ( is_numeric( $product ) ) {
			$product = wc_get_product( $product );
		}

		$prices_precision = wc_get_price_decimals();
		return array(
			'title'              => $product->get_name(),
			'id'                 => $product->get_id(),
			'created_at'         => $this->server->format_datetime( $product->get_date_created(), false, true ),
			'updated_at'         => $this->server->format_datetime( $product->get_date_modified(), false, true ),
			'type'               => $product->get_type(),
			'status'             => $product->get_status(),
			'downloadable'       => $product->is_downloadable(),
			'virtual'            => $product->is_virtual(),
			'permalink'          => $product->get_permalink(),
			'sku'                => $product->get_sku(),
			'price'              => wc_format_decimal( $product->get_price(), $prices_precision ),
			'regular_price'      => wc_format_decimal( $product->get_regular_price(), $prices_precision ),
			'sale_price'         => $product->get_sale_price() ? wc_format_decimal( $product->get_sale_price(), $prices_precision ) : null,
			'price_html'         => $product->get_price_html(),
			'taxable'            => $product->is_taxable(),
			'tax_status'         => $product->get_tax_status(),
			'tax_class'          => $product->get_tax_class(),
			'managing_stock'     => $product->managing_stock(),
			'stock_quantity'     => $product->get_stock_quantity(),
			'in_stock'           => $product->is_in_stock(),
			'backorders_allowed' => $product->backorders_allowed(),
			'backordered'        => $product->is_on_backorder(),
			'sold_individually'  => $product->is_sold_individually(),
			'purchaseable'       => $product->is_purchasable(),
			'featured'           => $product->is_featured(),
			'visible'            => $product->is_visible(),
			'catalog_visibility' => $product->get_catalog_visibility(),
			'on_sale'            => $product->is_on_sale(),
			'product_url'        => $product->is_type( 'external' ) ? $product->get_product_url() : '',
			'button_text'        => $product->is_type( 'external' ) ? $product->get_button_text() : '',
			'weight'             => $product->get_weight() ? wc_format_decimal( $product->get_weight(), 2 ) : null,
			'dimensions'         => array(
				'length' => $product->get_length(),
				'width'  => $product->get_width(),
				'height' => $product->get_height(),
				'unit'   => get_option( 'woocommerce_dimension_unit' ),
			),
			'shipping_required'  => $product->needs_shipping(),
			'shipping_taxable'   => $product->is_shipping_taxable(),
			'shipping_class'     => $product->get_shipping_class(),
			'shipping_class_id'  => ( 0 !== $product->get_shipping_class_id() ) ? $product->get_shipping_class_id() : null,
			'description'        => wpautop( do_shortcode( $product->get_description() ) ),
			'short_description'  => apply_filters( 'woocommerce_short_description', $product->get_short_description() ),
			'reviews_allowed'    => $product->get_reviews_allowed(),
			'average_rating'     => wc_format_decimal( $product->get_average_rating(), 2 ),
			'rating_count'       => $product->get_rating_count(),
			'related_ids'        => array_map( 'absint', array_values( wc_get_related_products( $product->get_id() ) ) ),
			'upsell_ids'         => array_map( 'absint', $product->get_upsell_ids() ),
			'cross_sell_ids'     => array_map( 'absint', $product->get_cross_sell_ids() ),
			'parent_id'          => $product->get_parent_id(),
			'categories'         => wc_get_object_terms( $product->get_id(), 'product_cat', 'name' ),
			'tags'               => wc_get_object_terms( $product->get_id(), 'product_tag', 'name' ),
			'images'             => $this->get_images( $product ),
			'featured_src'       => wp_get_attachment_url( get_post_thumbnail_id( $product->get_id() ) ),
			'attributes'         => $this->get_attributes( $product ),
			'downloads'          => $this->get_downloads( $product ),
			'download_limit'     => $product->get_download_limit(),
			'download_expiry'    => $product->get_download_expiry(),
			'download_type'      => 'standard',
			'purchase_note'      => wpautop( do_shortcode( wp_kses_post( $product->get_purchase_note() ) ) ),
			'total_sales'        => $product->get_total_sales(),
			'variations'         => array(),
			'parent'             => array(),
		);
	}    
    
	/**
	 * Get the images for a product or product variation
	 *
	 * @since 2.1
	 * @param WC_Product|WC_Product_Variation $product
	 * @return array
	 */
	private function get_images( $product ) {
		$images        = $attachment_ids = array();
		$product_image = $product->get_image_id();

		// Add featured image.
		if ( ! empty( $product_image ) ) {
			$attachment_ids[] = $product_image;
		}

		// Add gallery images.
		$attachment_ids = array_merge( $attachment_ids, $product->get_gallery_image_ids() );

		// Build image data.
		foreach ( $attachment_ids as $position => $attachment_id ) {

			$attachment_post = get_post( $attachment_id );

			if ( is_null( $attachment_post ) ) {
				continue;
			}

			$attachment = wp_get_attachment_image_src( $attachment_id, 'full' );

			if ( ! is_array( $attachment ) ) {
				continue;
			}

			$images[] = array(
				'id'         => (int) $attachment_id,
				'created_at' => $this->server->format_datetime( $attachment_post->post_date_gmt ),
				'updated_at' => $this->server->format_datetime( $attachment_post->post_modified_gmt ),
				'src'        => current( $attachment ),
				'title'      => get_the_title( $attachment_id ),
				'alt'        => get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ),
				'position'   => (int) $position,
			);
		}

		// Set a placeholder image if the product has no images set.
		if ( empty( $images ) ) {

			$images[] = array(
				'id'         => 0,
				'created_at' => $this->server->format_datetime( time() ), // Default to now.
				'updated_at' => $this->server->format_datetime( time() ),
				'src'        => wc_placeholder_img_src(),
				'title'      => __( 'Placeholder', 'woocommerce' ),
				'alt'        => __( 'Placeholder', 'woocommerce' ),
				'position'   => 0,
			);
		}

		return $images;
	}    
    
    
	/**
	 * Get the attributes for a product or product variation
	 *
	 * @since 2.1
	 * @param WC_Product|WC_Product_Variation $product
	 * @return array
	 */
	private function get_attributes( $product ) {

		$attributes = array();

		if ( $product->is_type( 'variation' ) ) {

			// variation attributes
			foreach ( $product->get_variation_attributes() as $attribute_name => $attribute ) {

				// taxonomy-based attributes are prefixed with `pa_`, otherwise simply `attribute_`
				$attributes[] = array(
					'name'   => wc_attribute_label( str_replace( 'attribute_', '', $attribute_name ) ),
					'slug'   => str_replace( 'attribute_', '', str_replace( 'pa_', '', $attribute_name ) ),
					'option' => $attribute,
				);
			}
		} else {

			foreach ( $product->get_attributes() as $attribute ) {
				$attributes[] = array(
					'name'      => wc_attribute_label( $attribute['name'] ),
					'slug'      => str_replace( 'pa_', '', $attribute['name'] ),
					'position'  => (int) $attribute['position'],
					'visible'   => (bool) $attribute['is_visible'],
					'variation' => (bool) $attribute['is_variation'],
					'options'   => $this->get_attribute_options( $product->get_id(), $attribute ),
				);
			}
		}

		return $attributes;
	}
	
	/**
	 * Get attribute options.
	 *
	 * @param int $product_id
	 * @param array $attribute
	 * @return array
	 */
	protected function get_attribute_options( $product_id, $attribute ) {
		if ( isset( $attribute['is_taxonomy'] ) && $attribute['is_taxonomy'] ) {
			return wc_get_product_terms( $product_id, $attribute['name'], array( 'fields' => 'names' ) );
		} elseif ( isset( $attribute['value'] ) ) {
			return array_map( 'trim', explode( '|', $attribute['value'] ) );
		}

		return array();
	}

	/**
	 * Get the downloads for a product or product variation
	 *
	 * @since 2.1
	 * @param WC_Product|WC_Product_Variation $product
	 * @return array
	 */
	private function get_downloads( $product ) {

		$downloads = array();

		if ( $product->is_downloadable() ) {

			foreach ( $product->get_downloads() as $file_id => $file ) {

				$downloads[] = array(
					'id'   => $file_id, // do not cast as int as this is a hash
					'name' => $file['name'],
					'file' => $file['file'],
				);
			}
		}

		return $downloads;
	}
    
   public function get_vitrina_products($seudonimo_t, $fields = null)
    {
        try {
   			// $id = $this->get_product_by_meta_custom( );
   			$query = $this->get_product_by_meta_custom( 'vitrina', $seudonimo_t );
   
   			// if ( empty( $query ) ) {
   			// 	throw new WC_API_Exception( 'woocommerce_api_invalid_product_sku', __( 'Invalid product SKU_Vitrina', 'woocommerce' ), 404 );
   			// }
            
            $products = array();
            
            foreach ( $query->posts as $product_id ) {

      			if ( ! $this->is_readable( $product_id ) ) {
      				continue;
      			}
      
      			$products[] = current( $this->get_product( $product_id, $fields ) );
      		}
      		
            
   			return array( 'products' => $products );
   		} catch ( WC_API_Exception $e ) {
   			return new WP_Error( $e->getErrorCode(), $e->getMessage(), array( 'status' => $e->getCode() ) );
   		}
    }    
    
    public function get_product_id_wcs_shop_server($wcs_shop_server_id, $fields = null)
    {
        try {
   			// $id = $this->get_product_by_meta_custom( );
   			$query = $this->get_product_by_meta_custom( 'id_servidor', $wcs_shop_server_id );
   
   			// if ( empty( $query ) ) {
   			// 	throw new WC_API_Exception( 'woocommerce_api_invalid_product_sku', __( 'Invalid product SKU_Vitrina', 'woocommerce' ), 404 );
   			// }
            
            $products = array();
            
            foreach ( $query->posts as $product_id ) {

      			if ( ! $this->is_readable( $product_id ) ) {
      				continue;
      			}
      
      			$products[] = current( $this->get_product( $product_id, $fields ) );
      		}
      		
            
   			return array( 'products' => $products );
   		} catch ( WC_API_Exception $e ) {
   			return new WP_Error( $e->getErrorCode(), $e->getMessage(), array( 'status' => $e->getCode() ) );
   		}
    }  
    
   private function get_product_by_meta_custom( $tipo, $productos ) {
         // Set base query arguments
   		$query_args = array(
   			'fields'      => 'ids',
   			'post_type'   => 'product',
   			'post_status' => 'publish',
   			'posts_per_page' => -1,
   			'meta_query'  => array(),
   		);
         
	         
	         if ( ! empty( $args['type'] ) ) {
	
				$types = explode( ',', $args['type'] );
	
				$query_args['tax_query'] = array(
					array(
						'taxonomy' => 'product_type',
						'field'    => 'slug',
						'terms'    => $types,
					),
				);
	
				unset( $args['type'] );
			}
         
			if ( ! is_array( $query_args['meta_query'] ) ) {
				$query_args['meta_query'] = array();
			}
			
			if($tipo === 'vitrina'){
				$query_args['meta_query'][] = array(
					'key'     => 'wcs_shop_server_tiendas_vitrinas',
					'value'   => $productos,
					'compare' => 'LIKE',
				);
			}
			
			if($tipo === 'id_servidor'){
				$query_args['meta_query'][] = array(
					'key'     => 'wcs_shop_server_id_product',//CAMBIAR ESTE META AGREGAR UN ID DE LA TIENDA AL MOMENTO DE INSTALAR EL PLUGIN CLIENTE
					'value'   => $productos,
					'compare' => '=',
				);
			}
			
			$query_args['post_type'] = array( 'product', 'product_variation' );
		

		$query_args = $this->merge_query_args( $query_args, $args );
		
		return new WP_Query( $query_args );
   }    
  
  public function import_product_by_id_clientes_1($id_cliente_in, $fields = null){
		 
		 
		/**
		 * Import products
		 *
		 * @return int Number of products imported
		 */
		 $imported_products_count = 0;
			
			//$this->log(__('Importing products...', 'cliente-1-dominio'));
			$vinculo = cliente_1_deserializer::getInstance()->get_value('vinculo_t') !='' ? esc_attr(cliente_1_deserializer::getInstance()->get_value('vinculo_t')) : '';
	   		$claveCliente = cliente_1_deserializer::getInstance()->get_value('clave_cliente_t') !='' ? esc_attr(cliente_1_deserializer::getInstance()->get_value('clave_cliente_t')) : '';
	   		$claveSecretaCliente = cliente_1_deserializer::getInstance()->get_value('clave_s_cliente_t') !='' ? esc_attr(cliente_1_deserializer::getInstance()->get_value('clave_s_cliente_t')) : '';
			
			$client = apply_filters('cliente_1_api_action', $vinculo, $claveCliente, $claveSecretaCliente);
			if(!empty($client)){
					
				
				try {
					$client->custom->setup('tienda');
					$products_clients = $client->custom->get('product/'.intval($id_cliente_in));
					// $products_clients = $client->products->get('',array( 'filter[sku]' => $sku_in));
				}catch(WC_API_Client_Exception $e) {
					echo 'Error: '.$e;
					wp_die();
				}
				
				// do {
					// if ( $this->import_stopped() ) {
					// 	break;
					// }
					// $products_clients = array_map('array_filter', $products_clients);	
					// $products_count = count($products_clients);
						if(count($products_clients['products'])>0){
							foreach ($products_clients['products'] as $key => $value ) {
								//verificar que no se vaya repetir el mismo sku
								if($this->get_product_by_sku($value['sku'])>0 && !empty($value['sku'])){
									continue;
								}
								$new_post_id = $this->import_product($value);
								
								// Increment the PrestaShop last imported product ID
								// update_option('fgp2wc_last_product_id', $product['id_product']);
			
								if ( $new_post_id ) {
									$imported_products_count++;
									
								// 	// Hook for doing other actions after inserting the post
								// 	// do_action('fgp2wc_post_import_product', $new_post_id, $product);
								}
							}
						}else {
							wp_cache_flush();
							return -1;
						}
					// $this->progressbar->increment_current_count($products_count);
				// } while ( ($products != null)  && ($products_count > 0) );
			}else {
				echo '<pre>ERROR HORROR</pre>';
			}	
			$client=null;
			wp_cache_flush();
			return $imported_products_count;
			// Recount the terms
			// $this->recount_terms();
			
			// $this->display_admin_notice(sprintf(_n('%d product imported', '%d products imported', $imported_products_count, 'cliente-1-dominio'), $imported_products_count));
			
    }

	/**
	* Import a product
	*
	* @param array $product Product data
	* @param int $language Language ID
	* @return int New post ID
	*/
	private function import_product($product) {
			$product_medias = array();
			$post_media = array();
			$product_types = $this->create_woocommerce_product_types();
			

			// Date
			$date = $product['created_at'];

			// Product images
			if ( true ) {

				$images = $product['images'];
				foreach ( $images as $image ) {
						// foreach ($key as $image){
						$image_name = !empty($image['title'])? $image['title'] : $product['title'];
						$image_src =  $image['src'] ; // Get the potential filenames
						$media_id = $this->guess_import_media($image_name, $image_src, $date, $image['id']);
						if ( $media_id !== FALSE ) {
							$product_medias[] = $media_id;
						}
					// }
				}
				// $this->media_count += count($product_medias); conteo de imagenes

				// Import content media
				$result = $this->import_media_from_content($product['description'], $date);
				$post_media = $result['media'];
				// $this->media_count += $result['media_count'];
			}

			// Product categories
			$categories_ids = array();
			$product_categories = $product['categories'];
			foreach ( $product_categories as $cat_value ) {
				if(count($this->categorias_importadas_productos()) > 0){
					foreach($this->categorias_importadas_productos() as $key => $categoria){
						if($categoria->name == $cat_value){
							$categories_ids[] = get_term_by('name',$cat_value, 'product_cat')->term_id;
						}else {
							$term = wp_insert_term( $cat_value, 'product_cat', [
								'description'=> $cat_value.' Category description imported',
								'slug' => strtolower ($cat_value).'-category' ]
							);
							if ( is_wp_error( $term ) ) {
								$term_id = isset($term->error_data['term_exists']) ? $term->error_data['term_exists']:  null;
							} else {
								$categories_ids[] = $term['term_id'];
							}
						}
					}
				}else {
					$term = wp_insert_term( $cat_value, 'product_cat', [
						'description'=> $cat_value.' Category description imported',
						'slug' => strtolower ($cat_value).'-category' ]
					);
					if ( is_wp_error( $term ) ) {
						$term_id = isset($term->error_data['term_exists']) ? $term->error_data['term_exists']:  null;
					} else {
						$categories_ids[] = $term['term_id'];
					}
				}
			}

			// Tags
			// $tags = $product['tags'];
			// if ( $this->plugin_options['meta_keywords_in_tags'] && !empty($product['meta_keywords']) ) {
			// 	$tags = array_merge($tags, explode(',', $product['meta_keywords']));
			// }
			// $this->import_tags($tags, 'product_tag');

			// Process content
			$content = isset($product['description'])? $product['description'] : '';
			$content = $this->process_content($content, $post_media);
			$excerpt = isset($product['short_description'])? $product['short_description'] : '';

			// Insert the post
			$new_post = array(
				'post_content'		=> $content,
				'post_date'			=> $date,
				'post_excerpt'		=> $excerpt,
				'post_status'		=> $product['status'],
				'post_title'		=> $product['title'],
				'post_name'			=> $product['title'],
				'post_type'			=> 'product',
				'tax_input'			=> array(
					'product_cat'	=> $categories_ids,
				),
			);

			// Hook for modifying the WordPress post just before the insert
			//$new_post = apply_filters('fgp2wc_pre_insert_product', $new_post, $product);
			
			$new_post_id = wp_insert_post($new_post);

			if ( $new_post_id ) {
				
				
				// Product type (simple or variable)
				//$product_type = $this->product_types['simple'];
						
                // Product type (simple or variable). comprobar si hay tallas disponible hacer nuevamente 
                
                if($product['type']=='variable'){
                    $product_type = $product_types['variable'];;
                    $manage_stock = 'no';
                    //Set default atributtes on variation product
                   // add_post_meta($new_post_id, '_default_attributes', array('talla' => trim($tallas[0])));
                }else if($product['type']=='simple'){
                    $product_type = $product_types['simple'];;
                    $manage_stock = 'yes';
                    add_post_meta($new_post_id, '_stock', $product['stock_quantity'], TRUE);
                }
                
                wp_set_object_terms($new_post_id, intval($product_type), 'product_type', TRUE);

				// Product galleries
				$medias_id = array();
				foreach ($product_medias as $media) {
					$medias_id[] = $media;
				}
				if ( true ) {
					// Don't include the first image into the product gallery
					array_shift($medias_id);
				}
				
				$gallery = implode(',', $medias_id);

				// Prices
				
				// SKU = Stock Keeping Unit
				$sku = $product['sku'];
				// if ( empty($sku) ) {
				// 	// return 0;
					
				// }

				// Stock
				$manage_stock = $product['managing_stock']? 'yes': 'no';
				//$stock_status = (!$this->plugin_options['stock_management'] || ($product['quantity'] > 0))? 'instock': 'outofstock';
				$stock_status =  ($product['in_stock']) ? 'instock': 'outofstock';

				// Backorders
				// $backorders = $this->allow_backorders($product['out_of_stock']);

				// Add the meta data
				add_post_meta($new_post_id, '_visibility', $product['visible'], TRUE);
				add_post_meta($new_post_id, '_stock_status', $stock_status, TRUE);
				add_post_meta($new_post_id, '_regular_price', $product['regular_price'], TRUE);
				add_post_meta($new_post_id, '_price', $product['price'], TRUE);
				add_post_meta($new_post_id, '_sale_price', $product['sale_price'], TRUE);
				// add_post_meta($new_post_id, '_sale_price_dates_from', $reduction_from, TRUE);
				// add_post_meta($new_post_id, '_sale_price_dates_to', $reduction_to, TRUE);
				add_post_meta($new_post_id, '_featured', $product['featured'], TRUE);
				// add_post_meta($new_post_id, '_weight', floatval($product['weight']), TRUE);
				// add_post_meta($new_post_id, '_length', floatval($product['depth']), TRUE);
				add_post_meta($new_post_id, '_width', floatval($product['dimensions']['width']), TRUE);
				add_post_meta($new_post_id, '_height', floatval($product['dimensions']['height']), TRUE);
				add_post_meta($new_post_id, '_sku', $sku, TRUE);
				add_post_meta($new_post_id, '_manage_stock', $manage_stock, TRUE);
				// add_post_meta($new_post_id, '_backorders', $backorders, TRUE);
				add_post_meta($new_post_id, '_product_image_gallery', $gallery, TRUE);
				add_post_meta($new_post_id, '_virtual', $product['virtual'], TRUE);
				add_post_meta($new_post_id, '_downloadable', $product['downloadable'], TRUE);
				add_post_meta($new_post_id, 'total_sales', $product['total_sales'], TRUE);
				add_post_meta($new_post_id, 'wcs_shop_server_id_product', $product['id_servidor'], TRUE);
				
				 if($product['type']=='variable'){
					//parent_id para manejar el id del post nuevo
	                $parent_id = $new_post_id;
	                    // agrego el nuevo atributo 
	                    
	                    $i=0;
	                    foreach ($product['attributes'] as $p_atributos) {
	                    	 $attibutos[$p_atributos['name']] = Array(
		                        'name'=> $p_atributos['name'],
		                        'value'=> implode("|",$p_atributos['options']),
		                        'position' => $i,
		                        'is_visible' => '1',
		                        'is_variation' => '1',
		                        'is_taxonomy' => '0'
		                    );
		                    $i++;
                    		// add_post_meta( $parent_id,'_product_attributes',$attibutos, TRUE);
	                    }
	                    update_post_meta( $parent_id,'_product_attributes',$attibutos, TRUE);
	
	                $i=0;
	                foreach($product['variations'] as $variacion){
	                	
		                    $variation = array(
		                        'post_title'   => 'Atributos ' . $variacion['id'] . ' for #'.$parent_id ,
		                        'post_name' => 'product-' . $parent_id . '-variation-' . $i,
		                        'post_status'  => 'publish',
		                        'post_parent'  => $parent_id,
		                        'post_type'    => 'product_variation'
		                    );
		                    
		                    $attID = wp_insert_post( $variation );
		                    
		                    foreach ($variacion['attributes'] as $value) {
		                    	update_post_meta($attID, 'attribute_'.$value['name'], $value['option']);	
		                    }
		                    
		                    update_post_meta($attID, '_manage_stock', $variacion['managing_stock']=='parent' ?  $manage_stock : ($variacion['managing_stock']) ? 'yes' : 'no');
		                    update_post_meta($attID, '_stock', $variacion['stock_quantity']);
		                    update_post_meta($attID, '_sku', ($variacion['sku'] != $product['sku']) ? $variacion['sku'] : '' );
		                    update_post_meta($attID, '_stock_status',($variacion['in_stock']) ? 'instock' : 'outofstock');
		                    update_post_meta($attID, '_price', $variacion['price']);
		                    update_post_meta($attID, '_regular_price', $variacion['regular_price']);
		                    update_post_meta($attID, '_sale_price', $variacion['sale_price']);
		                    update_post_meta($attID, 'wcs_shop_server_id_product', $variacion['id_servidor']);
		                    
	                    $i=$i+1;
	                }
				 }
				// Add the reference value
				// if ( ($this->plugin_options['sku'] != 'reference') && !empty($product['reference']) ) {
				// 	add_post_meta($new_post_id, 'reference', $product['reference'], TRUE);
				// }
				
				// Add the EAN-13 value
				// if ( ($this->plugin_options['sku'] != 'ean13') && !empty($product['ean13']) ) {
				// 	add_post_meta($new_post_id, 'ean13', $product['ean13'], TRUE);
				// }
				
				// Add links between the post and its medias
				$this->add_post_media($new_post_id, $product_medias, $date, TRUE);
				$this->add_post_media($new_post_id, $this->get_attachment_ids($post_media), $date, FALSE);

				// Add the PrestaShop ID as a post meta
				// add_post_meta($new_post_id, '_fgp2wc_old_product_id', $product['id_product'], TRUE);
				
				// Hook for doing other actions after inserting the post
				// do_action('fgp2wc_post_insert_product', $new_post_id, $product, $language);
			}
			return $new_post_id;
		}

		public function categorias_importadas_productos(){
			$cat_args = array(
                'orderby'    => 'name',
                'order'      => 'asc',
                'hide_empty' => false,
            );
             
            return get_terms( 'product_cat', $cat_args );
		}
		
		/**
		 * Create the WooCommerce product types
		 *
		 * @return array Product types
		 */
		private function create_woocommerce_product_types() {
			$tab_types = array();
			$taxonomy = 'product_type';
			$product_types = array(
				'simple',
				'grouped',
				'variable',
				'external',
			);
			
			foreach ( $product_types as $product_type ) {
				$term = get_term_by('slug', $product_type, $taxonomy);
				if ( !empty($term) ) {
					$tab_types[$product_type] = $term->term_id;
				} else {
					$new_term = wp_insert_term($product_type, $taxonomy);
					if ( !is_wp_error($new_term) ) {
						$tab_types[$product_type] = $new_term['term_id'];
					}
				}
			}
			return $tab_types;
		}


		/**
		 * Import a media by guessing its name
		 * 
		 * @param string $image_name Image name
		 * @param array $image_filenames List of potential filenames
		 * @param date $date Media creation date
		 * @param int $image_id Original image ID (optional)
		 * @return int media ID
		 */
		public function guess_import_media($image_name, $image_src, $date='', $image_id=0) {
			// Optimization to get the right image filename
			$media_id = FALSE;
			if ( FALSE ) {
				$media_id = $this->import_media($image_name, $image_src[$this->image_filename_key], $date, array(), $image_id);
			}
			if ( $media_id === FALSE ) {
				// foreach ( $image_filenames as $key => $image_filename ) {
					if ( $image_src ) {
							$media_id = $this->import_media($image_name, $image_src, $date, array(), $image_id);
						// if ( $media_id !== FALSE ) {
						// 	$this->image_filename_key = $key;
						// 	break; // the media has been imported, we don't continue with the other potential filenames
						// }
					}
				// }
			}
			return $media_id;
		}
		
		/**
		 * Returns the imported post ID corresponding to a meta key and value
		 *
		 * @since 3.3.0
		 * 
		 * @param string $meta_key Meta key
		 * @param string $meta_value Meta value
		 * @return int WordPress post ID
		 */
		public function get_wp_post_id_from_meta($meta_key, $meta_value) {
			global $wpdb;

			$sql = "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = '$meta_key' AND meta_value = '$meta_value' LIMIT 1";
			$post_id = $wpdb->get_var($sql);
			return $post_id;
		}		

		/**
		 * Import a media
		 *
		 * @param string $name Image name
		 * @param string $filename Image URL
		 * @param date $date Date (optional)
		 * @param array $options Options (optional)
		 * @param int $image_id Original image ID (optional)
		 * @return int attachment ID or FALSE
		 */
		public function import_media($name, $file_src, $date='', $options=array(), $image_id=0) {
			
			// Check if the media is already imported
			$attachment_id = $this->get_wp_post_id_from_meta('old_image_id', $image_id);
			
			if ( !$attachment_id ) {

				if ( empty($date) || ($date == '0000-00-00 00:00:00') ) {
					$date = date('Y-m-d H:i:s');
				}
				$import_external = true /* ($this->plugin_options['import_external'] == 1) || (isset($options['force_external']) && $options['force_external'] )*/;
				
				preg_match('@[^/?#]*\.[^/?#]*(\?.*)?(\#.*)?$@', $file_src, $filename);
				// print_r($filename[0]);
				$filename = urldecode($filename[0]);// for filenames with spaces or accents
				
				$filetype = wp_check_filetype($filename);
				if ( empty($filetype['type']) || ($filetype['type'] == 'text/html') ) { // Unrecognized file type
					echo 'error tipo ok';
					return FALSE;
				}

				// // Upload the file from the PrestaShop web site to WordPress upload dir
				// if ( preg_match('/^http/', $filename) ) {
				// 	if ( $import_external || /* External file*/ preg_match('#^' . $this->plugin_options['url'] . '#', $filename) /* Local file*/){
				// 		$old_filename = $filename;
				// 	} else {
				// 		return FALSE;
				// 	}
				// } elseif ( preg_match('#^/img#', $filename) ) {
				// 	$old_filename = untrailingslashit($url) . $filename;
				// } else {
					$old_filename = /*untrailingslashit($url) . '/img/' . $filename;*/ $file_src;
				// }
				$old_filename = str_replace(" ", "%20", $old_filename); // for filenames with spaces

				// Get the upload path
				$upload_path = $this->upload_dir($filename, $date);

				// Make sure we have an uploads directory.
				if ( !wp_mkdir_p($upload_path) ) {
					//$this->display_admin_error(sprintf(__("Unable to create directory %s", 'client-1-domain'), $upload_path));
					return FALSE;
				}

				$new_filename = $filename;
				if ( /*$this->plugin_options['import_duplicates'] == 1 */ true) {
					// Images with duplicate names
					$new_filename = preg_replace('#.*img/#', '', $new_filename);
					$new_filename = str_replace('http://', '', $new_filename);
					$new_filename = str_replace('/', '_', $new_filename);
				}

				$basename = basename($new_filename);
				$extension = substr(strrchr($basename, '.'), 1);
				$basename_without_extension = preg_replace('/(\.[^.]+)$/', '', $basename);
				$post_title = $name;
				$new_full_filename = $upload_path . '/' . $this->format_filename(/*$basename_without_extension . '-' .*/ $name) . '.' . $extension;

//				print "Copy \"$old_filename\" => $new_full_filename<br />";
				if ( ! @$this->remote_copy($old_filename, $new_full_filename) ) {
//					$error = error_get_last();
//					$error_message = $error['message'];
//					$this->display_admin_error("Can't copy $old_filename to $new_full_filename : $error_message");
					echo 'ERROR REMOTE COPY';
					return FALSE;
				}

				// Image Alt
				$image_alt = '';
				if ( !empty($name) ) {
					$image_alt = wp_strip_all_tags(stripslashes($name), TRUE);
				}

				// GUID
				$upload_dir = wp_upload_dir();
				$guid = str_replace($upload_dir['basedir'], $upload_dir['baseurl'], $new_full_filename);
				$attachment_id = $this->insert_attachment($post_title, $basename, $new_full_filename, $guid, $date, $filetype['type'], $image_alt, $image_id);
			}
			// print_r($attachment_id);
			return $attachment_id;
		}

		/**
		 * Determine the media upload directory
		 * 
		 * @since 2.3.0
		 * 
		 * @param string $filename Filename
		 * @param date $date Date
		 * @return string Upload directory
		 */
		public function upload_dir($filename, $date) {
			$upload_dir = wp_upload_dir(strftime('%Y/%m', strtotime($date)));
			$use_yearmonth_folders = get_option('uploads_use_yearmonth_folders');
			if ( $use_yearmonth_folders ) {
				$upload_path = $upload_dir['path'];
			} else {
				$short_filename = preg_replace('#.*img/#', '/', $filename);
				if ( strpos($short_filename, '/') != 0 ) {
					$short_filename = '/' . $short_filename; // Add a slash before the filename
				}
				$upload_path = $upload_dir['basedir'] . untrailingslashit(dirname($short_filename));
			}
			return $upload_path;
		}
		
		/**
		 * Format a filename
		 * 
		 * @since 3.7.3
		 * 
		 * @param string $filename Filename
		 * @return string Formated filename
		 */
		public function format_filename($filename) {
			$filename = $this->convert_to_latin($filename);
			$filename = preg_replace('/%.{2}/', '', $filename); // Remove the encoded characters
			$filename = sanitize_file_name($filename);
			return $filename;
		}		
		
		/**
		 * Convert string to latin
		 */
		public function convert_to_latin($string) {
			$string = self::greek_to_latin($string); // For Greek characters
			$string = self::cyrillic_to_latin($string); // For Cyrillic characters
			return $string;
		}
		
		/**
		 * Convert Greek characters to latin
		 */
		private static function greek_to_latin($string) {
			static $from = array('Α','Ά','Β','Γ','Δ','Ε','Έ','Ζ','Η','Θ','Ι','Κ','Λ','Μ','Ν','Ξ','Ο','Π','Ρ','Σ','Τ','Υ','Φ','Χ','Ψ','Ω','α','ά','β','γ','δ','ε','έ','ζ','η','ή','θ','ι','ί','ϊ','κ','λ','μ','ν','ξ','ο','ό','π','ρ','ς','σ','τ','υ','ύ','φ','χ','ψ','ω','ώ','ϑ','ϒ','ϖ');
			static $to = array('A','A','V','G','D','E','E','Z','I','TH','I','K','L','M','N','X','O','P','R','S','T','Y','F','CH','PS','O','a','a','v','g','d','e','e','z','i','i','th','i','i','i','k','l','m','n','x','o','o','p','r','s','s','t','y','y','f','ch','ps','o','o','th','y','p');
			return str_replace($from, $to, $string);
		}

		/**
		 * Convert Cyrillic (Russian) characters to latin
		 */
		private static function cyrillic_to_latin($string) {
			static $from = array('ж',  'ч',  'щ',   'ш',  'ю',  'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я', 'Ж',  'Ч',  'Щ',   'Ш',  'Ю',  'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я');
			static $to = array('zh', 'ch', 'sht', 'sh', 'yu', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'y', 'x', 'q', 'Zh', 'Ch', 'Sht', 'Sh', 'Yu', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'c', 'Y', 'X', 'Q');
			return str_replace($from, $to, $string);
		}
		
		/**
		 * Copy a remote file
		 * in replacement of the copy function
		 * 
		 * @param string $url URL of the source file
		 * @param string $path destination file
		 * @return boolean
		 */
		public function remote_copy($url, $path) {
			
			// Don't copy the file if already copied
			if (/* !$this->plugin_options['force_media_import'] && */ file_exists($path) && (filesize($path) > 0) ) {
				return TRUE;
			}
			
			$response = wp_remote_get($url, array(
				'timeout'		=> $this->plugin_options['timeout'],
				'sslverify'		=> FALSE,
				'redirection'	=> 0,
			)); // Uses WordPress HTTP API
			
			if ( is_wp_error($response) ) {
				trigger_error($response->get_error_message(), E_USER_WARNING);
				return FALSE;
			} elseif ( $response['response']['code'] != 200 ) {
				trigger_error($response['response']['message'], E_USER_WARNING);
				return FALSE;
			} else {
				file_put_contents($path, wp_remote_retrieve_body($response));
				return TRUE;
			}
		}
		/**
		 * Save the attachment and generates its metadata
		 * 
		 * @since 2.3.0
		 * 
		 * @param string $attachment_title Attachment name
		 * @param string $basename Original attachment filename
		 * @param string $new_full_filename New attachment filename with path
		 * @param string $guid GUID
		 * @param date $date Date
		 * @param string $filetype File type
		 * @param string $image_alt Image description
		 * @param int $image_id Image ID
		 * @return int|FALSE Attachment ID or FALSE
		 */
		public function insert_attachment($attachment_title, $basename, $new_full_filename, $guid, $date, $filetype, $image_alt='', $image_id=0) {
			$post_name = sanitize_title($attachment_title);
			
			// If the attachment does not exist yet, insert it in the database
			$attachment_id = 0;
			$attachment = $this->get_attachment_from_name($post_name);
			if ( $attachment ) {
				$attached_file = basename(get_attached_file($attachment->ID));
				if ( $attached_file == $basename ) { // Check if the filename is the same (in case where the legend is not unique)
					$attachment_id = $attachment->ID;
				}
			}
			if ( $attachment_id == 0 ) {
				$attachment_data = array(
					'guid'				=> $guid, 
					'post_date'			=> $date,
					'post_mime_type'	=> $filetype,
					'post_name'			=> $post_name,
					'post_title'		=> $attachment_title,
					'post_status'		=> 'inherit',
					'post_content'		=> '',
				);
				$attachment_id = wp_insert_attachment($attachment_data, $new_full_filename);
				if ( !empty($image_id) ) {
					add_post_meta($attachment_id, 'old_image_id', $image_id, TRUE);
				} else {
					add_post_meta($attachment_id, '_imported', 1, TRUE); // To delete the imported attachments
				}
			}
			
			if ( !empty($attachment_id) ) {
				if ( preg_match('/image/', $filetype) ) { // Images
					// you must first include the image.php file
					// for the function wp_generate_attachment_metadata() to work
					require_once(ABSPATH . 'wp-admin/includes/image.php');
					$attach_data = wp_generate_attachment_metadata( $attachment_id, $new_full_filename );
					wp_update_attachment_metadata($attachment_id, $attach_data);

					// Image Alt
					if ( !empty($image_alt) ) {
						update_post_meta($attachment_id, '_wp_attachment_image_alt', addslashes($image_alt)); // update_post_meta expects slashed
					}
				}
				return $attachment_id;
			} else {
				return FALSE;
			}
		}
		
		/**
		 * Check if the attachment exists in the database
		 *
		 * @param string $name
		 * @return object Post
		 */
		private function get_attachment_from_name($name) {
			$name = preg_replace('/\.[^.]+$/', '', basename($name));
			$r = array(
				'name'			=> $name,
				'post_type'		=> 'attachment',
				'numberposts'	=> 1,
			);
			$posts_array = get_posts($r);
			if ( is_array($posts_array) && (count($posts_array) > 0) ) {
				return $posts_array[0];
			}
			else {
				return FALSE;
			}
		}
		
		/**
		 * Import post medias from content
		 *
		 * @param string $content post content
		 * @param date $post_date Post date (for storing media)
		 * @param array $options Options
		 * @return array:
		 * 		array media: Medias imported
		 * 		int media_count:   Medias count
		 */
		public function import_media_from_content($content, $post_date, $options=array()) {
			$media = array();
			$media_count = 0;
			$matches = array();
			$alt_matches = array();
			
			if ( preg_match_all('#<(img|a)(.*?)(src|href)="(.*?)"(.*?)>#', $content, $matches, PREG_SET_ORDER) > 0 ) {
				if ( is_array($matches) ) {
					foreach ($matches as $match ) {
						$filename = $match[4];
						$other_attributes = $match[2] . $match[5];
						// Image Alt
						$image_alt = '';
						if (preg_match('#alt="(.*?)"#', $other_attributes, $alt_matches) ) {
							$image_alt = wp_strip_all_tags(stripslashes($alt_matches[1]), TRUE);
						}
						$attach_id = $this->import_media($image_alt, $filename, $post_date, $options, esc_attr(cliente_1_deserializer::getInstance()->get_value('vinculo_t')));
						if ( $attach_id !== FALSE ) {
							$media_count++;
							$attachment = get_post($attach_id);
							if ( !is_null($attachment) ) {
								$media[$filename] = array(
									'id'	=> $attach_id,
									'name'	=> $attachment->post_name,
								);
							}
						}
					}
				}
			}
			return array(
				'media'			=> $media,
				'media_count'	=> $media_count
			);
		}

		/**
		 * Process the post content
		 *
		 * @param string $content Post content
		 * @param array $post_media Post medias
		 * @return string Processed post content
		 */
		public function process_content($content, $post_media) {
			
			if ( !empty($content) ) {
				$content = str_replace(array("\r", "\n"), array('', ' '), $content);
				
				// Replace page breaks
				$content = preg_replace("#<hr([^>]*?)class=\"system-pagebreak\"(.*?)/>#", "<!--nextpage-->", $content);
				
				// Replace media URLs with the new URLs
				$content = $this->process_content_media_links($content, $post_media);
			}

			return $content;
		}
		
		/**
		 * Replace media URLs with the new URLs
		 *
		 * @param string $content Post content
		 * @param array $post_media Post medias
		 * @return string Processed post content
		 */
		private function process_content_media_links($content, $post_media) {
			$matches = array();
			$matches_caption = array();
			
			if ( is_array($post_media) ) {
				
				// Get the attachments attributes
				$attachments_found = FALSE;
				foreach ( $post_media as $old_filename => &$media_var ) {
					$post_media_name = $media_var['name'];
					$attachment = $this->get_attachment_from_name($post_media_name);
					if ( $attachment ) {
						$media_var['attachment_id'] = $attachment->ID;
						$media_var['url_old_filename'] = urlencode($old_filename); // for filenames with spaces
						if ( preg_match('/image/', $attachment->post_mime_type) ) {
							// Image
							$image_src = wp_get_attachment_image_src($attachment->ID, 'full');
							$media_var['new_url'] = $image_src[0];
							$media_var['width'] = $image_src[1];
							$media_var['height'] = $image_src[2];
						} else {
							// Other media
							$media_var['new_url'] = wp_get_attachment_url($attachment->ID);
						}
						$attachments_found = TRUE;
					}
				}
				if ( $attachments_found ) {
				
					// Remove the links from the content
					$this->post_link_count = 0;
					$this->post_link = array();
					$content = preg_replace_callback('#<(a) (.*?)(href)=(.*?)</a>#i', array($this, 'remove_links'), $content);
					$content = preg_replace_callback('#<(img) (.*?)(src)=(.*?)>#i', array($this, 'remove_links'), $content);
					
					// Process the stored medias links
					$first_image_removed = false;
					foreach ($this->post_link as &$link) {
						
						// Remove the first image from the content
						if ( /*($this->plugin_options['first_image'] == 'as_featured') &&*/ !$first_image_removed && preg_match('#^<img#', $link['old_link']) ) {
							$link['new_link'] = '';
							$first_image_removed = TRUE;
							continue;
						}
						$new_link = $link['old_link'];
						$alignment = '';
						if ( preg_match('/(align="|float: )(left|right)/', $new_link, $matches) ) {
							$alignment = 'align' . $matches[2];
						}
						if ( preg_match_all('#(src|href)="(.*?)"#i', $new_link, $matches, PREG_SET_ORDER) ) {
							$caption = '';
							foreach ( $matches as $match ) {
								$old_filename = $match[2];
								$link_type = ($match[1] == 'src')? 'img': 'a';
								if ( array_key_exists($old_filename, $post_media) ) {
									$media = $post_media[$old_filename];
									if ( array_key_exists('new_url', $media) ) {
										if ( (strpos($new_link, $old_filename) > 0) || (strpos($new_link, $media['url_old_filename']) > 0) ) {
											$new_link = preg_replace('#('.$old_filename.'|'.$media['url_old_filename'].')#', $media['new_url'], $new_link, 1);
											
											if ( $link_type == 'img' ) { // images only
												// Define the width and the height of the image if it isn't defined yet
												if ((strpos($new_link, 'width=') === FALSE) && (strpos($new_link, 'height=') === FALSE)) {
													$width_assertion = isset($media['width'])? ' width="' . $media['width'] . '"' : '';
													$height_assertion = isset($media['height'])? ' height="' . $media['height'] . '"' : '';
												} else {
													$width_assertion = '';
													$height_assertion = '';
												}
												
												// Caption shortcode
												if ( preg_match('/class=".*caption.*?"/', $link['old_link']) ) {
													if ( preg_match('/title="(.*?)"/', $link['old_link'], $matches_caption) ) {
														$caption_value = str_replace('%', '%%', $matches_caption[1]);
														$align_value = ($alignment != '')? $alignment : 'alignnone';
														$caption = '[caption id="attachment_' . $media['attachment_id'] . '" align="' . $align_value . '"' . $width_assertion . ']%s' . $caption_value . '[/caption]';
													}
												}
												
												$align_class = ($alignment != '')? $alignment . ' ' : '';
												$new_link = preg_replace('#<img(.*?)( class="(.*?)")?(.*) />#', "<img$1 class=\"$3 " . $align_class . 'size-full wp-image-' . $media['attachment_id'] . "\"$4" . $width_assertion . $height_assertion . ' />', $new_link);
											}
										}
									}
								}
							}
							
							// Add the caption
							if ( $caption != '' ) {
								$new_link = sprintf($caption, $new_link);
							}
						}
						$link['new_link'] = $new_link;
					}
					
					// Reinsert the converted medias links
					$content = preg_replace_callback('#__fg_link_(\d+)__#', array($this, 'restore_links'), $content);
				}
			}
			return $content;
		}
		
		/**
		 * Remove all the links from the content and replace them with a specific tag
		 * 
		 * @param array $matches Result of the preg_match
		 * @return string Replacement
		 */
		private function remove_links($matches) {
			$this->post_link[] = array('old_link' => $matches[0]);
			return '__fg_link_' . $this->post_link_count++ . '__';
		}
		
		/**
		 * Get the IDs of the medias
		 *
		 * @param array $post_media Post medias
		 * @return array Array of attachment IDs
		 */
		public function get_attachment_ids($post_media) {
			$attachments_ids = array();
			if ( is_array($post_media) ) {
				foreach ( $post_media as $media ) {
					$attachment = $this->get_attachment_from_name($media['name']);
					if ( !empty($attachment) ) {
						$attachments_ids[] = $attachment->ID;
					}
				}
			}
			return $attachments_ids;
		}
		
		/**
		 * Add a link between a media and a post (parent id + thumbnail)
		 *
		 * @param int $post_id Post ID
		 * @param array $post_media Post medias
		 * @param array $date Date
		 * @param boolean $set_featured_image Set the featured image?
		 */
		public function add_post_media($post_id, $post_media, $date, $set_featured_image=TRUE) {
			$thumbnail_is_set = FALSE;
			if ( is_array($post_media) ) {
				foreach ( $post_media as $media ) {
					$attachment = get_post($media);
					if ( !empty($attachment) && ($attachment->post_type == 'attachment') ) {
						$attachment->post_parent = $post_id; // Attach the post to the media
						$attachment->post_date = $date ;// Define the media's date
						wp_update_post($attachment);

						// Set the featured image. If not defined, it is the first image of the content.
						if ( $set_featured_image && !$thumbnail_is_set ) {
							set_post_thumbnail($post_id, $attachment->ID);
							$thumbnail_is_set = TRUE;
						}
					}
				}
			}
		}		
		
		
		/**
		 * Recalculate the terms counters
		 * 
		 */
		private function recount_terms() {
			$taxonomy_names = wc_get_attribute_taxonomy_names();
			foreach ( $taxonomy_names as $taxonomy ) {
				$terms = get_terms($taxonomy, array('hide_empty' => 0));
				$termtax = array();
				foreach ( $terms as $term ) {
					$termtax[] = $term->term_taxonomy_id; 
				}
				wp_update_term_count($termtax, $taxonomy);
			}
		}
		
		/**
		 * Chequear si ya existe el mismo sku
		 * 
		 */
		 function get_product_by_sku( $sku ) {

			global $wpdb;
			
			$product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku ) );
		
			if ( $product_id ) return new WC_Product( $product_id );
		
			return null;
			
		}
		
  
}