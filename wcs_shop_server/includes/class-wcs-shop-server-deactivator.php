<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/includes
 * @author     Your Name <email@example.com>
 */
class wcs_shop_server_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.0.1
	 */
	public static function deactivate() {

	}

}
