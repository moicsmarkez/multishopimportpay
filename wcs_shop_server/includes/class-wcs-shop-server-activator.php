<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      0.0.1
 *
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.0.1
 * @package    wcs_shop_server
 * @subpackage wcs_shop_server/includes
 * @author     Your Name <email@example.com>
 */
class wcs_shop_server_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.0.1
	 */
	public static function activate() {

	}

}
