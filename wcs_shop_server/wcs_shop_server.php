<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.google.com
 * @since             0.0.1
 * @package           wcs-shop-server
 *
 * @wordpress-plugin
	Plugin Name:		 Servidor de tiendas  
 * Plugin URI:        http://example.com/wcs-shop-server-uri/
 * Description:		 servidor de tiendas
 * Version: 			 0.0.1
 * Author:  			 moicsmarkez
 * Author URI:        http://www.google.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wcs-shop-server
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wcs-shop-server-activator.php
 */
function activate_wcs_shop_server() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wcs-shop-server-activator.php';
	wcs_shop_server_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wcs-shop-server-deactivator.php
 */
function deactivate_wcs_shop_server() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wcs-shop-server-deactivator.php';
	wcs_shop_server_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wcs_shop_server' );
register_deactivation_hook( __FILE__, 'deactivate_wcs_shop_server' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wcs-shop-server.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wcs_shop_server() {

	$plugin = new wcs_shop_server();
	$plugin->run();

}
	run_wcs_shop_server();

if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {

	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-wcs-shop-server-admin.php' );
//	add_action( 'plugins_loaded', array( 'Stock_Manager_Admin', 'get_instance' ) );

}


