<?php
/**
 * 
 * @package /include
 */
  
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if (!class_exists('WD_API_LOADER')) {
    /**
     *
     * @package /include
     * @author Skatox
     */
    class WD_API_LOADER
    {
        public function init(){
          add_action( 'woocommerce_api_loaded', array( $this, 'load' ) );
        }
        public function load(){
        	require_once plugin_dir_path( dirname( __FILE__ ) ).'includes/wc-api-wcs-client-shop.php';
        	add_filter( 'woocommerce_api_classes', array( $this, 'register' ), 10, 1 );
        }
        public function register( $api_classes=array() ){
            $entra = array('WC_API_wcs_client_shop',);
            return array_merge($api_classes, $entra);
         }
    }
}
$wc_custom_api = new WD_API_LOADER();
$wc_custom_api->init();