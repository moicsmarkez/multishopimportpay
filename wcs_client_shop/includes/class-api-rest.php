<?php

 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 
 if ( ! class_exists( 'wcs_client_shop_api' ) ) :
    
      class wcs_client_shop_api{
    
         protected static $instance = NULL;
         
         public static function getInstance() {
            NULL === self::$instance and self::$instance = new self;
            return self::$instance;
         }
         
         
         public static function empezar_api($link, $cliente_key, $cliente_secret_key){
            require_once( 'lib/woocommerce-api.php' );
            
            $options = array(
            	'debug'           => false,
            	'return_as_array' => true,
            	'validate_url'    => false,
            	'timeout'         => 30,
            	'ssl_verify'      => false,
            );
            
            try {
            
              // $client = new WC_API_Client( 'https://personal1-moicsmarkez.c9users.io', 'ck_6cb23d87ae6a6a22f77e909de5342502c885e253', 'cs_745e480f9bc12398128da5af2471945483d5b4c8', $options );
               $client = new WC_API_Client( $link, $cliente_key, $cliente_secret_key, $options ); 
               
               return $client;
               
               //$prodcut_ok = $client->products->get('',array( 'filter[sku]' => 'woo-s-005' ));
               //$prodcut_ok = $client->products->update($prodcut_ok['products'][0]['id'], array( 'stock_quantity' => 6 ));
               
               //print_r($prodcut_ok);
            
            } catch ( WC_API_Client_Exception $e ) {
            
            	if ( $e instanceof WC_API_Client_HTTP_Exception ) {
               	//return $e->get_request();
             		//print_r( $e->get_response() );
            	}
            	//return $e->getMessage() . PHP_EOL;
            	return 'ERROR';
            //	echo $e->getCode() . PHP_EOL;
            }
      
         }
      }

endif;

return wcs_client_shop_api::getInstance();