<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://example.com
 * @since      0.1.5
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.1.5
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/includes
 * @author     Your Name <email@example.com>
 */
class wcs_client_shop_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.1.5
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wcs-shop-server',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
