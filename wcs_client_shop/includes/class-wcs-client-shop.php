<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      0.1.5
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      0.1.5
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/includes
 * @author     Your Name <email@example.com>
 */
 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit

if ( ! class_exists( 'wcs_client_shop' ) ) :
	
	class wcs_client_shop {
	
		/**
		 * The loader that's responsible for maintaining and registering all hooks that power
		 * the plugin.
		 *
		 * @since    0.1.5
		 * @access   protected
		 * @var      wcs_client_shop_Loader    $loader    Maintains and registers all hooks for the plugin.
		 */
		protected $loader;
	
		/**
		 * The unique identifier of this plugin.
		 *
		 * @since    0.1.5
		 * @access   protected
		 * @var      string    $wcs_client_shop    The string used to uniquely identify this plugin.
		 */
		protected $wcs_client_shop;
	
		/**
		 * The current version of the plugin.
		 *
		 * @since    0.1.5
		 * @access   protected
		 * @var      string    $version    The current version of the plugin.
		 */
		protected $version;
	
		/**
		 * Define the core functionality of the plugin.
		 *
		 * Set the plugin name and the plugin version that can be used throughout the plugin.
		 * Load the dependencies, define the locale, and set the hooks for the admin area and
		 * the public-facing side of the site.
		 *
		 * @since    0.1.5
		 */
		public function __construct() {
	
			$this->wcs_client_shop = 'wcs-client-shop';
			$this->version = '0.1.5';
	
			$this->load_dependencies();
			//$this->set_locale(); ESTOS SE DEBEN COLOCAR DESPUES
			$this->define_admin_hooks();
			//$this->define_public_hooks(); IGUAL QUE AQUI HAY QUE COLOCARLOS DESPUES
	
		}
	
		/**
		 * Load the required dependencies for this plugin.
		 *
		 * Include the following files that make up the plugin:
		 *
		 * - wcs_client_shop_Loader. Orchestrates the hooks of the plugin.
		 * - wcs_client_shop_i18n. Defines internationalization functionality.
		 * - wcs_client_shop_Admin. Defines all hooks for the admin area.
		 * - wcs_client_shop_Public. Defines all hooks for the public side of the site.
		 *
		 * Create an instance of the loader which will be used to register the hooks
		 * with WordPress.
		 *
		 * @since    0.1.5
		 * @access   private
		 */
		private function load_dependencies() {
	
			/**
			 * The class responsible for orchestrating the actions and filters of the
			 * core plugin.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wcs-client-shop-loader.php';
	
			/**
			 * The class responsible for defining internationalization functionality
			 * of the plugin.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wcs-client-shop-i18n.php';
			
			/**
			 * Clase responsable para el manejo del api dentro 
			 * del plugin.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-api-rest.php';
	
			/**
			 * The class responsible for defining all actions that occur in the admin area.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wcs-client-shop-admin.php';
			/**
			 * Clase para incorporar nuevos endpoints en el api de woocommerce.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/woocommerce-extend-api.php';
			/**
			 * The class responsible for defining all actions that occur in the public-facing
			 * side of the site.
			 */
		//	require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wcs-client-shop-public.php';
	
			$this->loader = new wcs_client_shop_Loader();
	
		}
	
		/**
		 * Define the locale for this plugin for internationalization.
		 *
		 * Uses the wcs_client_shop_i18n class in order to set the domain and to register the hook
		 * with WordPress.
		 *
		 * @since    0.1.5
		 * @access   private
		 */
		private function set_locale() {
	
			$plugin_i18n = new wcs_client_shop_i18n();
	
			$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'wcs-client-shop' );
	
		}
	 
		/**
		 * Register all of the hooks related to the admin area functionality 
		 * of the plugin.
		 *
		 * @since    0.1.5
		 * @access   private 
		 */
		private function define_admin_hooks() {
	
			$plugin_admin = new wcs_client_shop_Admin( $this->get_wcs_client_shop(), $this->get_version() );
			
			$this->loader->add_action( 'init',$plugin_admin, 'wpse_remove_hooks', 20 );
			$this->loader->add_action( 'template_redirect',$plugin_admin, 'redirect_necesario', 20 ); 
			$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
			$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
			$this->loader->add_action( 'save_post', $plugin_admin, 'guardarIDservidor', 10, 3);
			$this->loader->add_filter( 'woocommerce_api_product_response', $plugin_admin, 'get_tienda_tag', 20, 4);
			$this->loader->add_filter( 'wcs_client_shop_api_action', wcs_client_shop_api::getInstance(), 'empezar_api', 20, 3 );
			$this->loader->add_action( 'woocommerce_proceed_to_checkout', $plugin_admin, 'cambia_url_to_checkout', 20);
			
    	   	$estados = array(0 => 'pending', 'customer_on_hold_order' => 'on-hold', 'failed_order' => 'failed', 'cancelled_order' => 'cancelled','customer_processing_order' => 'processing' , 'customer_completed_order' => 'completed', 'customer_refunded_order' => 'refunded');
			foreach($estados as $key => $value){
			    $this->loader->add_action( 'woocommerce_order_status_'.$value, $plugin_admin, 'orden_cambia_estado', 10, 1);    
			}
		}
	
		/**
		 * Register all of the hooks related to the public-facing functionality
		 * of the plugin.
		 *
		 * @since    0.1.5
		 * @access   private
		 */
		private function define_public_hooks() {
	
			$plugin_public = new wcs_client_shop_Public( $this->get_wcs_client_shop(), $this->get_version() );
	
			$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
			$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
	
		}
	
		/**
		 * Run the loader to execute all of the hooks with WordPress.
		 *
		 * @since    0.1.5
		 */
		public function run() {
			$this->loader->run();
		}
	
		/**
		 * The name of the plugin used to uniquely identify it within the context of
		 * WordPress and to define internationalization functionality.
		 *
		 * @since     0.1.5
		 * @return    string    The name of the plugin.
		 */
		public function get_wcs_client_shop() {
			return $this->wcs_client_shop;
		}
	
		/**
		 * The reference to the class that orchestrates the hooks with the plugin.
		 *
		 * @since     0.1.5
		 * @return    wcs_client_shop_Loader    Orchestrates the hooks of the plugin.
		 */
		public function get_loader() {
			return $this->loader;
		}
	
		/**
		 * Retrieve the version number of the plugin.
		 *
		 * @since     0.1.5
		 * @return    string    The version number of the plugin.
		 */
		public function get_version() {
			return $this->version;
		}
	
	}
  
endif;

return new wcs_client_shop();