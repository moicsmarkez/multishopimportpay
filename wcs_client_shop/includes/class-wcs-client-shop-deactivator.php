<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/includes
 * @author     Your Name <email@example.com>
 */
class wcs_client_shop_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.5
	 */
	public static function deactivate() {

	}

}
