<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      0.1.5
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.1.5
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/includes
 * @author     Your Name <email@example.com>
 */
class wcs_client_shop_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1.5
	 */
	
	
	public static function activate() {
		// Check PHP Version and deactivate & die if it doesn't meet minimum requirements.
		if ( !function_exists('mcrypt_encrypt') ) {
			deactivate_plugins( plugin_basename( __FILE__ ) );
			wp_die( 'Este plugin requiere instalar la extencion de mcrypt-php en tu servidor.  Sorry about that.' );
			exit();
		}
		
		include_once( plugin_dir_path( __FILE__ ) . '../shared/class-deserializer.php' );	
		if(wcs_client_shop_deserializer::getInstance()->get_value('nombre_t') =='' || empty(wcs_client_shop_deserializer::getInstance()->get_value('nombre_t') ) ){  update_option( 'nombre_t', 'http://wcssl.co');}
		if(wcs_client_shop_deserializer::getInstance()->get_value('id_tienda_wcs_client_shop') != '' || !empty(wcs_client_shop_deserializer::getInstance()->get_value('id_tienda_wcs_client_shop'))){
			//NADA POR LOS MOMENTOS	
		}else {
			if(strpos(get_option('home'), 'http://') === 0){
				$striniado = str_replace('http://', '', get_option('home'));	
			}else if(strpos(get_option('home'), 'https://') === 0){
				$striniado = str_replace('https://', '', get_option('home')); 	
			}
			add_option( 'id_tienda_wcs_client_shop',  str_replace('/', '_', $striniado));	
		}
		self::creador_servidor_id(wcs_client_shop_deserializer::getInstance()->get_value('id_tienda_wcs_client_shop'));
	}
	
	//FUNCION 
		private static function creador_servidor_id($identificador_host){
			global $wpdb; 
			$post_lista =  query_posts(array('post_type' => 'product', 'posts_per_page' => -1));
			$i=count($wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->postmeta} WHERE meta_key='%s'",'wcs_shop_server_id_product_'.$identificador_host)));
			if(!empty($post_lista)) {
				foreach ( $post_lista as $post ) {

					if ( is_object( $post ) ) {
						$id = $post->ID;
					} else {
						$id = $post;
					}

                    $product_type = ( $terms = wp_get_object_terms( $id, 'product_type' ) ) ? sanitize_title( current( $terms )->name ) : apply_filters( 'default_product_type', 'simple' );

					if ( $product_type == 'variable' ) {
						if(get_post_meta($id,'wcs_shop_server_id_product_'.$identificador_host, true) =='' || empty(get_post_meta($id,'wcs_shop_server_id_product_'.$identificador_host, true)) ){
							update_post_meta( $id, 'wcs_shop_server_id_product_'.$identificador_host, $i+1);
							$i=$i+2;
						}else {
							$i++;
						}
						
						$args_v = array(
                            'post_type'      => 'product_variation',
                            'post_status'    => array( 'private', 'publish' ),
                            'posts_per_page' => -1, 
                            'orderby'        => array( 'menu_order' => 'ASC', 'ID' => 'DESC' ),
                            'post_parent'    => $id,
                            'fields' => 'ids'
                        );

						$variations = get_children( $args_v );
						foreach ( $variations as $ids ) {
							if ( ! empty( $ids ) && (get_post_meta($ids,'wcs_shop_server_id_product_'.$identificador_host, true) =='' || empty(get_post_meta($ids,'wcs_shop_server_id_product_'.$identificador_host, true))) ) {
                                update_post_meta( $ids, 'wcs_shop_server_id_product_'.$identificador_host, $i );
                                $i++;
							}
						}
					}else{
						if(get_post_meta($id,'wcs_shop_server_id_product_'.$identificador_host, true) =='' || empty(get_post_meta($id,'wcs_shop_server_id_product_'.$identificador_host, true)) ){
							update_post_meta( $id, 'wcs_shop_server_id_product_'.$identificador_host, $i+1);
							$i++;
						}
					}
				}
			}
		}

}
