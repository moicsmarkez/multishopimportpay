
<div class="wrap">
            <h1 style="font-size: 30px;font-weight: 900;text-shadow: 3px 3px 5px #929292;" ><?php echo esc_html( get_admin_page_title() ); ?></h1>
            <h3><?php  _e('Configuracion de la API de tienda Servidor','wc-total-shop'); ?></h3>

            <?php  do_action( 'get_wcts_settings_messages' ); 
            
            ?>
            
            <form method="post" action="<?php echo esc_html( admin_url( 'admin-post.php' ) ); ?>" id="form_server" >
                
              <table class="settings-class-s1">
                <tbody>
                    <tr>
                        <td>Servidor</td>
                    </tr>
                    <tr>
                        <td>
                            <select name="nombre_t" id="nombre_t"  style="width: 100%;" >
                                <option value="http://60mad.net/marketplace/" <?php if(esc_attr( wcs_client_shop_deserializer::getInstance()->get_value('nombre_t')) === 'http://60mad.net/marketplace/'){echo 'selected="selected"';} ?> >Marketplace</option>
                                <option value="https://personal1-moicsmarkez.c9users.io/" <?php if(esc_attr( wcs_client_shop_deserializer::getInstance()->get_value('nombre_t')) === 'https://personal1-moicsmarkez.c9users.io/'){echo 'selected="selected"';} ?> > Servidor Prueba</option></option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" name="check_server_nonce" value="<?php echo wp_create_nonce('api_checking_server') ?>" />
            <?php
                wp_nonce_field( 'wcs-client-shop-settings-save', 'wcs-client-shop-prior' );
                @submit_button(); 
            ?>
            </form>

</div>
