jQuery(document).ready(function() {
    
        jQuery('#form_server input').on('input', function () {
            var boton = jQuery('#check_server');
            boton.prop("disabled", false); 
            boton.val('Check');
        });
    
    
        jQuery('body').on('click', 'div.wrap #check_server', function () {
	    var data = jQuery('#form_server :input').serializeArray();
        
        var boton = jQuery('#check_server');
        var action = 'admin-ajax.php?action=check_server_api';//colocar el hook para chekear
        var method = 'post';

        jQuery.ajax({
            url: action,
            method: method,
            data: data,
        beforeSend: function() {
            console.log('Updating Field');
            jQuery(this).attr('disabled', 'disable');
            jQuery('#form_server').block({message: null,overlayCSS: {background: '#e4e4e4',opacity: 0.6}});
        },
        success : function( response ) {
             console.log('Success ' );
             if(response.success){
                 boton.removeClass('button-error');
                 boton.val('OK');
                 boton.attr('disabled', 'disable');
             }else {
                 boton.addClass('button-error');
                 boton.val(response.data);
             }
        },
        }).done(function (data) {
            jQuery(this).removeAttr('disabled');
            jQuery('#form_server').unblock();
        })
    });

    
});
