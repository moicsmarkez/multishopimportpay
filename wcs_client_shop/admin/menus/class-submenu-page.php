<?php
/**
 * The menus functionality of the plugin.
 *
 * @link       http://example.com
 * @since      0.0.1
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/admin/menus
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/admin/menus
 * @author     Your Name <email@example.com>
 */
 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 if ( ! class_exists( 'wcs_client_shop_submenu_page' ) ) :
 
 class wcs_client_shop_submenu_page{
        protected static $instance = NULL;

        public static function getInstance() {
            NULL === self::$instance and self::$instance = new self;
            return self::$instance;
        }
 
    function __construct(){
     //   add_action( 'admin_enqueue_scripts', array(__CLASS__, 'add_my_stylesheet'));
    }
	
	public static function submenu_page_() {
            include_once( plugin_dir_path( __FILE__ ) .'../views/settings.php' );
    }

 }
 endif;
 
 return wcs_client_shop_submenu_page::getInstance();
