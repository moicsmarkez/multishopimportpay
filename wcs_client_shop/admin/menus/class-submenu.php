<?php

/**
 * The menus functionality of the plugin.
 *
 * @link       http://example.com
 * @since      0.1.5
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/admin/menus
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/admin/menus
 * @author     Your Name <email@example.com>
 */
 
 if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
 
 if ( ! class_exists( 'wcs_client_shop_submenu' ) ) :

include_once( plugin_dir_path( __FILE__ ) . 'class-submenu-page.php' );
 
 class wcs_client_shop_submenu{
 
   	 
	 public function __construct( ) {
		add_action('admin_menu', array(__CLASS__,'wcts_submenu_'),99);
	}

	public static function wcts_submenu_() {
            $clase =wcs_client_shop_submenu_page::getInstance();
            $mainpage = add_submenu_page( 'woocommerce', 'Wcs Servidor', 'Wcs Servidor', 'manage_options', 'wcs_client_shop_opciones', array($clase,'submenu_page_') ); 
    }

     
 }
 endif;
 
 return new wcs_client_shop_submenu();
