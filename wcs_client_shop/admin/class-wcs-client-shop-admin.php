<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      0.1.5
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/admin
 * @author     Your Name <email@example.com>
 */


 
class wcs_client_shop_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1.5
	 * @access   private
	 * @var      string    $wcs_client_shop    The ID of this plugin.
	 */
	private $wcs_client_shop;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1.5
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1.5
	 * @param      string    $wcs_client_shop       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $wcs_client_shop, $version ) {
		$this->requires_incl();
		$this->wcs_client_shop = $wcs_client_shop;
		$this->version = $version;
		
	}

	private function requires_incl(){
		include_once( plugin_dir_path( __FILE__ ) . 'menus/class-submenu.php' );
		include_once( plugin_dir_path( __FILE__ ) . '../shared/class-deserializer.php' );
		include_once( plugin_dir_path( __FILE__ ) . '../includes/class-serializer.php' );
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    0.1.5
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in wcs_client_shop_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The wcs_client_shop_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->wcs_client_shop, plugin_dir_url( __FILE__ ) . 'css/wcs-client-shop-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    0.1.5
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in wcs_client_shop_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The wcs_client_shop_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->wcs_client_shop, plugin_dir_url( __FILE__ ) . 'js/wcs-client-shop-admin.js', array( 'jquery' ), $this->version, false );
		wp_register_script('jquery-blockUi', plugin_dir_url( __FILE__ ) .'js/jquery.blockUI.js', array('jquery'));
		wp_enqueue_script('jquery-blockUi');    

	}

		//EL ALGORITMO DE IMPORTACION DEBE SER IGUAL AL ALGORITMO DE IMPORTACION EN EL API-WEB-SERVICE-PERSONALIZADO
		//LAS NUEVAS FUNCIONES A PARTIR DE AQUI ====>>>
	
		public function redirect_necesario(){
			if(is_shop()){
				if(isset($_GET['key_cliente_order'])){
					$order_id = wc_get_order_id_by_order_key($_GET['key_cliente_order']);
					$order = wc_get_order($order_id);
					wp_redirect($order->get_checkout_order_received_url());
					exit();
				}
			}else if ( is_order_received_page() )  {
				// echo 'ok';
				return ;
			}else if(is_checkout()){
				 wp_redirect(WC()->cart->get_cart_url());
				 exit();
			}
			
		}
		 
		public function wpse_remove_hooks(){
			remove_action('woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20);
			if(is_user_logged_in() && get_user_meta( get_current_user_id(), '_wcs_shop_server_cliente_user_reset_cart', true ) == true && !is_admin()){
				
				wc_setcookie( 'woocommerce_items_in_cart', 1 );
        		wc_setcookie( 'woocommerce_cart_hash', md5( json_encode( WC()->cart->empty_cart() ) ) );
        		do_action( 'woocommerce_set_cart_cookies', true );
        		update_user_meta(get_current_user_id(),'_wcs_shop_server_cliente_user_reset_cart', false);
			}
		}
		
		public function cambia_url_to_checkout(){
			global $woocommerce;
    		$items = $woocommerce->cart->get_cart();
			
			$url_servidor = esc_attr( wcs_client_shop_deserializer::getInstance()->get_value('nombre_t'));
			$id_cliente_bd = wcs_client_shop_deserializer::getInstance()->get_value('id_tienda_wcs_client_shop');
			
			if( is_cart() || is_checkout() ) {
			    foreach($items as $contenido){
					 $prod_id = ( isset( $contenido['variation_id'] ) && $contenido['variation_id'] != 0 ) ? $contenido['variation_id'] : $contenido['product_id'];
					 $contenido_carrito[] =array( 
					 	'wcs_shop_server_product_id' => intval(get_post_meta($prod_id, 'wcs_shop_server_id_product_'.$id_cliente_bd, true)),
					 	'cantidad' => $contenido['quantity'],
					 );	 
				}
				
				$cupones_id = array();
				if(!empty($woocommerce->cart->get_coupons())){
				    foreach($woocommerce->cart->get_coupons() as $key => $cupon){
				        $cupones_id[$cupon->get_code()]= $cupon->get_id();
				    }
				}
				
			    $contenido_carrito['site_url'] = get_permalink( wc_get_page_id( 'shop' ));
                $contenido_carrito['host_id'] = $id_cliente_bd ;
				$contenido_carrito['cupon_descuento']= $cupones_id;
				if(is_user_logged_in()){$contenido_carrito['logged']= wp_get_current_user()->user_email;}
				// $contenido_carrito = str_replace("\"","'",base64_encode(serialize($contenido_carrito)));
				   $contenido_carrito_md5 = md5(serialize($contenido_carrito));
				   $contenido_carrito = $this->encrypt_decrypt('encrypt',serialize($contenido_carrito), $contenido_carrito_md5);
			}
			
	        ?>
	        <input type="hidden" name="p_cart" value="<?php echo $contenido_carrito; ?>" />
	        <input type="hidden" name="carrito_key" value="<?php echo $contenido_carrito_md5; ?>" />
	        <a href="#" id="proceder-compra" class="checkout-button button alt wc-forward"><?php _e( 'Proceed to Checkout', 'woocommerce' ); ?></a>
	        
	         <script type="text/javascript">            
	            jQuery(function(){
	                jQuery( 'body').on( 'click', '#proceder-compra', function() {
	                	jQuery('body').block({ message: 'Cargando, por favor espere!', baseZ: 2035,centerY: 0,
	                		css: { 
					            border: 'none', 
					            padding: '15px', 
					            top: '280px',
					            backgroundColor: '#000', 
					            '-webkit-border-radius': '10px', 
					            '-moz-border-radius': '10px', 
					            opacity: .5, 
					            color: '#fff' 
				        	} });
	                    var formulario = jQuery(this).parents().find('form');
	                    formulario.find('input[name="s"]').remove()//MOMENTANEO
	                    formulario.prop('action', '<?php echo $url_servidor ?>' ).prop('method', 'post').append(jQuery('input[name="p_cart"]').append(jQuery('input[name="carrito_key"]')));
	                    formulario.submit();
	                });
	            });
	        </script>
	        <?php   
		}
	
	public static function guardarIDservidor($post_id, $post, $update ){
          
         if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
         
         global $wpdb; 
         
         if ($post->post_type != 'product' && $post->post_type != 'product_variation'){
           return;
         }
         
         $id_cliente_bd = wcs_client_shop_deserializer::getInstance()->get_value('id_tienda_wcs_client_shop');
         
         $product_type = ($terms = wp_get_object_terms( $post_id, 'product_type' ))  ?  sanitize_title( current( $terms )->name ) : apply_filters( 'default_product_type', 'simple' );
		
		 if($product_type  === 'variable'){
           $ids_wcs_shop_server = count($wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->postmeta} WHERE meta_key='%s'",'wcs_shop_server_id_product_'.$id_cliente_bd)));
           if(get_post_meta($post_id,'wcs_shop_server_id_product_'.$id_cliente_bd, true) =='' || empty(get_post_meta($post_id,'wcs_shop_server_id_product_'.$id_cliente_bd, true)) ){
              update_post_meta( $post_id, 'wcs_shop_server_id_product_'.$id_cliente_bd, $ids_wcs_shop_server+1);
              $ids_wcs_shop_server=$ids_wcs_shop_server+2;
           }else {
              $ids_wcs_shop_server=$ids_wcs_shop_server+1;
           }
              
					$args_v = array(
                        'post_type'      => 'product_variation',
                        'post_status'    => array( 'private', 'publish' ),
                        'posts_per_page' => -1, 
                        'orderby'        => array( 'menu_order' => 'ASC', 'ID' => 'DESC' ),
                        'post_parent'    => $post_id,
                        'fields' => 'ids'
                    );

					$variations = get_children( $args_v );
					foreach ( $variations as $ids ) {
						if ( ! empty( $ids ) && (get_post_meta($ids,'wcs_shop_server_id_product_'.$id_cliente_bd, true) =='' || empty(get_post_meta($ids,'wcs_shop_server_id_product_'.$id_cliente_bd, true))) ) {
                            update_post_meta( $ids, 'wcs_shop_server_id_product_'.$id_cliente_bd, $ids_wcs_shop_server );
                            $ids_wcs_shop_server++;
						}
					}
					
					$variations=null; $args_v=null; 
        }else if($product_type  != 'variable'){
           if(get_post_meta($post_id,'wcs_shop_server_id_product_'.$id_cliente_bd, true) =='' || empty(get_post_meta($post_id,'wcs_shop_server_id_product_'.$id_cliente_bd, true)) ){
               $ids_wcs_shop_server = count($wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->postmeta} WHERE meta_key='%s'",'wcs_shop_server_id_product_'.$id_cliente_bd)));
               update_post_meta( $post_id, 'wcs_shop_server_id_product_'.$id_cliente_bd, $ids_wcs_shop_server+1);
           }
        }
         
      }
      
      public function get_tienda_tag($product_data, $product, $fields, $server){
      	
      		$id_cliente_bd = wcs_client_shop_deserializer::getInstance()->get_value('id_tienda_wcs_client_shop');
      		
			$product_data['id_servidor']  = ($product_data['type']==='variation') ? get_post_meta($product_data['id'],'wcs_shop_server_id_product_'.$id_cliente_bd, true) : get_post_meta($product_data['id'],'wcs_shop_server_id_product_'.$id_cliente_bd, true);
			if($product_data['type']==='variable'){
				$i=0;
				foreach($product_data['variations'] as $variaciones){
					$product_data['variations'][$i]['id_servidor'] = get_post_meta($variaciones['id'],'wcs_shop_server_id_product_'.$id_cliente_bd,true);$i++;
				}
			}
			
			return $product_data;
	}
	
	public function orden_cambia_estado($order_id){
	   	$order = wc_get_order($order_id);
	   	
	    if(strval($order->get_status()) != 'pending'){
		    if(wcs_client_shop_deserializer::getInstance()->get_value('_wcs_client_shop_factura') ==='nop'){
			    $estados = array( 'failed_order' => 'failed', 'cancelled_order' => 'cancelled','customer_processing_order' => 'processing' , 'customer_completed_order' => 'completed', 'customer_refunded_order' => 'refunded');
				if ( ! empty( WC()->mailer()->get_emails() ) ) {
			        foreach ( WC()->mailer()->get_emails() as $mail ) {
			            if ( $mail->id == strval(array_search($order->get_status(),$estados)) ) {
			        	    $mail->enabled = 'no';
			            }
			        }
			    }
		     }
	       }
   }
   
   /**
	 * simple method to encrypt or decrypt a plain text string
	 * initialization vector(IV) has to be the same when encrypting and decrypting
	 * 
	 * @param string $action: can be 'encrypt' or 'decrypt'
	 * @param string $string: string to encrypt or decrypt
	 *
	 * @return string
	*/
	private function encrypt_decrypt($action, $string, $md5_string) {
	    $output = false;
	    
	    $dirty = array("+", "/", "=");
		$clean = array("_PIOPIO_", "_SAPOAPO_", "_ECOECO_");
	    
	    if ( $action == 'encrypt' ) {
	    	$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
		    $secret_iv = mcrypt_create_iv($iv_size, $this->md5_hex_to_dec($md5_string));
		    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $md5_string, utf8_encode($string), MCRYPT_MODE_ECB, $secret_iv );
		    $encrypted_string = base64_encode($encrypted_string);
	    	
	        $output = str_replace($dirty, $clean, $encrypted_string);
	    } else if( $action == 'decrypt' ) {
	    	$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
		    $secret_iv = mcrypt_create_iv($iv_size, $this->md5_hex_to_dec($md5_string));
		    
		    $string = base64_decode(str_replace($clean, $dirty, $string));
		    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $md5_string, $string, MCRYPT_MODE_ECB, $secret_iv);
		    
	        $output = $decrypted_string;
	    }
	    return $output;
	}
	
	private function md5_hex_to_dec($hex_str){
	    $arr = str_split($hex_str, 4);
	    foreach ($arr as $grp) {
	        $dec[] = str_pad(hexdec($grp), 5, '0', STR_PAD_LEFT);
	    }
	    return implode('', $dec);
	}
}
