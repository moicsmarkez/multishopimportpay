<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      0.1.5
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/share
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    wcs_client_shop
 * @subpackage wcs_client_shop/share
 * @author     Your Name <email@example.com>
 */

 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit


if ( ! class_exists( 'wcs_client_shop_deserializer' ) ) :
 
 
class wcs_client_shop_deserializer {


        protected static $instance = NULL;

        public static function getInstance() {
            NULL === self::$instance and self::$instance = new self;
            return self::$instance;
        }

	/**
	 * Retrieves the value for the option identified by the specified key. If
	 * the option value doesn't exist, then an empty string will be returned.
	 *
	 * @param  string $option_key The key used to identify the option.
	 * @return string             The value of the option or an empty string.
	 */
	public function get_value( $option_key ) {
		return get_option( $option_key, '' );
	}
}
 
endif;

return wcs_client_shop_deserializer::getInstance();
